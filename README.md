The package `RealCertify` aims at providing a full suite of hybrid algorithms for computing certificates of non-negativity based on numerical software for solving linear
matrix inequalities. The module `univsos` handles the univariate case and the module  `multivsos` is designed for the multivariate case.

### Download and execution
`RealCertify` is maintained as a Gricad-Gitlab repository at the address https://gricad-gitlab.univ-grenoble-alpes.fr/magronv/RealCertify

It can be obtained with the command:

$ git clone https://gricad-gitlab.univ-grenoble-alpes.fr/magronv/RealCertify

From the RealCertify/ directory, launch Maple and execute the following command:

`read "realcertify.mm":`

# univsos
Sums of squares decomposition of univariate nonnegative polynomials

## Description
`univsos` computes sums of squares (SOS) decompositions of univariate nonnegative polynomials with rational coefficients. The library includes two distinct algorithms: 

- `univsos1`, which relies on root isolation, quadratic under approximations of positive polynomials and square-free decomposition.
- `univsos2`, which relies on root isolation of perturbed positive  polynomials and square-free decomposition.
- `univsos3`, which relies on sums of squares approximation (via semidefinite programming) of perturbed positive polynomials and square-free decomposition.

## Installation instructions
### Prerequisites
`univsos` has been tested with `Maple2016` and requires: 
- For univsos2: the external `PARI/GP` software available at the address http://pari.math.u-bordeaux.fr/download.html
- For univsos3: the external `SDPA` software (SDP solver) available at the address https://sourceforge.net/projects/sdpa/files/sdpa/sdpa_7.3.8.tar.gz
- For univsos3: the external `SDPA-GMP` software (arbitrary-precision SDP solver) available at the address https://github.com/nakatamaho/sdpa-gmp

`$ git clone https://github.com/nakatamaho/sdpa-gmp`
`$ cd sdpa-gmp/`
`$ ./configure`
`$ make`


### Execution and benchmarks

Let us consider the polynomial f := 1 + X + X^2 + X^3 + X^4. 
To compute a sums of squares decomposition of f, you can:


1) rely on univsos1

`f := 1 + X + X^2 + X^3 + X^4: sos:=univsos1(f);`

                      sos := [1, 0, 1, (X + 1) (X - 1/2), 3/4, X + 1, 1, -X]

The output is a list [c1,p1,...,cr,pr], where each ci is a rational number and each pi is a rational polynomial such that f admits the following weigthed SOS decomposition:

`f  = c1*p1^2 + ... + cr*pr^2`

You can verify afterwards that this yields a valid nonnegativty certificate of f with the following command:

`s := 0: for i from 1 to nops(sos)/2 do s := s + sos[2*i-1]*sos[2*i]^2 od: expand (f -s);`


<!---
                       sos := [[1, [1, X/2 + 1, 0]], [X, [0, 0, 0]], [0, [1, X + 1/2, 1/2]]]

The output is a list [(p1, (a1, b1, c1)),..., (pr, (ar, br, cr))], where each pi is a rational polynomial, ai*bi^2 + ci is a rational polynomial of degree at most 2, and such that f admits the Horner-like decomposition:

`f  = p1^2* [ p2^2* [ ... [pr^2 + ar * br^2 + cr]] + a2*b2^2 + c2] + a1*b1^2 + c1`

You can verify afterwards that this yields a valid nonnegativty certificate of f with the following command:

`expand(f - foldr((_e, a) -> _e[1]^2 * a + _e[2][1]*_e[2][2]^2 + _e[2][3], 1, op(sos)));`
-->

2) rely on univsos2

`f := 1 + X + X^2 + X^3 + X^4: sos := univsos2(f);`

                  2                      23 X   11  377      55             2                                               
    sos := [7/8, X  + 9/16 X - 3/4, 7/8, ---- + --, ----, 1, ---, X, 7/64, X , 9/1024, X + 1/2, 1/64, X (X + 1/2)]
  
                                          16    16  4096     256
                                         
                                         

The output is a list [c1,p1,...,cr,pr], where each ci is a rational number and each pi is a rational polynomial such that f admits the following weigthed SOS decomposition:

`f  = c1*p1^2 + ... + cr*pr^2`

You can verify afterwards that this yields a valid nonnegativty certificate of f with the following command:

`s := 0: for i from 1 to nops(sos)/2 do s := s + sos[2*i-1]*sos[2*i]^2 od: expand (f -s);`


3) rely on univsos3

`f := 1 + X + X^2 + X^3 + X^4: sos := sos3(f,X);`


The output and verification procedures are the same as for univsos2.


#### Benchmarks from the paper https://hal.archives-ouvertes.fr/ensl-00445343v2/document (Section 6)
#### univsos1

`BenchSOSitv(f1,g1,a1,b1);`

`BenchSOSitv(f3,g3,a3,b3);`

`BenchSOSitv(f4,g4,a4,b4);`

`BenchSOSitv(f5,g5,a5,b5);`

`BenchSOSitv(f6,g6,a6,b6);`

`BenchSOSitv(f7,g7,a7,b7);`

`BenchSOSitv(f8,g8,a8,b8);`

`BenchSOSitv(f9,g9,a9,b9);`

`BenchSOSitv(f10,g10,a10,b10);`

#### univsos2

`BenchSOSitv2(f1,g1,a1,b1):`

`BenchSOSitv2(f3,g3,a3,b3):`

`BenchSOSitv2(f4,g4,a4,b4):`

`BenchSOSitv2(f5,g5,a5,b5):`

`BenchSOSitv2(f6,g6,a6,b6):`

`BenchSOSitv2(f7,g7,a7,b7):`

`BenchSOSitv2(f8,g8,a8,b8):`

`BenchSOSitv2(f9,g9,a9,b9):`

`BenchSOSitv2(f10,g10,a10,b10):`

#### univsos3

`BenchSOSitv3(f1,g1,a1,b1,65,40,200,30,30):`

`BenchSOSitv3(f4,g4,a4,b4,120,30,200,40,40):`

`BenchSOSitv3(f5,g5,a5,b5,240,100,2000,100,100):`

`BenchSOSitv3(f6,g6,a6,b6,100,30,200,30,30):`

`BenchSOSitv3(f7,g7,a7,b7,150,100,300,30,30):`

`BenchSOSitv3(f8,g8,a8,b8,80,40,200,30,30):`

`BenchSOSitv3(f9,g9,a9,b9,80,50,200,30,30):`

`BenchSOSitv3(f10,g10,a10,b10,100,40,300,30,30):`

#### Benchmarks for nonnegative power sums of increasing degrees 1 + X + X^2 + ... + X^n
#### univsos1

`BenchSOSsum();`

#### univsos2

`BenchSOSsum2(2);`

#### Benchmarks for modified Wilkinson polynomials 1 + (X-1)^2...(X-n)^2
#### univsos1

`BenchWilkinson();`

#### univsos2

`BenchWilkinson2(2);`

#### Benchmarks for modified Mignotte polynomials X^n + 2 (101 X - 1)^2
#### univsos1

`BenchMignotte();`

#### univsos2

`BenchMignotte2(2);`

#### Benchmarks for modified Mignotte polynomials X^n + 2 (101 X - 1)^(n-2)
#### univsos1

`BenchMignotteN();`

#### univsos2
`BenchMignottedN2(2);`

#### Benchmarks for modified Mignotte polynomials (X^n + 2 (101 X - 1)^2) (X^n + 2*((101 +1/101)X - 1)^2)
#### univsos1

`BenchMignotteProd();`

#### univsos2

`BenchMignottedProd2(2);`

# multivsos
Sums of squares decomposition of multivariate nonnegative polynomials

## Description
`multivsos` is a Maple library for computation of sums of squares (SOS) decompositions of multivariate nonnegative polynomials with rational coefficients in the (un)-constrained case. 

In the unconstrained case, `multivsos` implements a hybrid numeric-symbolic algorithm computing exact rational SOS decompositions for polynomials lying in the interior of the SOS cone. It computes an approximate SOS decomposition for a perturbation of the input polynomial with an arbitrary-precision semidefinite programming (SDP) solver. An exact SOS decomposition is obtained thanks to the perturbation terms. 

In the constrained case, `multivsos` allows to compute weighted SOS decompositions for polynomials positive over basic compact semialgebraic sets.

## Installation instructions
### Prerequisites
`multivsos` has been tested with `Maple2016` and requires: 

- the `convex` Maple package available at the address https://www.math.uwo.ca/faculty/franz/convex/

- the external `SDPA` software (SDP solver) available at the address https://sourceforge.net/projects/sdpa/files/sdpa/sdpa_7.3.8.tar.gz. You need to add the following line to your mapleinit file:

`libname := libname, "$PATH_TO_CONVEX_PACKAGE/convex.mla":`

- the external `SDPA-GMP` software (arbitrary-precision SDP solver) available at the address https://github.com/nakatamaho/sdpa-gmp. To install it, run the following commands in your favorite shell:

`$ git clone https://github.com/nakatamaho/sdpa-gmp`
`$ cd sdpa-gmp/`
`$ ./configure`
`$ make`

### Execution and benchmarks

For both unconstrained and constrained problems, the `multivsos` procedure relies on a recursive procedure, called `multivsos_internal`.

#### Unconstrained problems

In the unconstrained case, the `multivsos_internal` procedure takes as input:

- the initial polynomial f 

In addition, `multivsos_internal` has the following keyword parameters:

- `epsilon`: the perturbation magnitude (default value = 10)
- `precSVD`: the precision of numerical Cholesky's decomposition (default value = 10)
- `precSDP`: the number of precision digits for the SDP solver (default value = 200)
- `epsStar`: the number of precision digits for the termination criterion of the SDP solver (default value = 30)
- `precOut`: the rounding precision for the output of the SDP solver (default value = 30)
- `precIn`: the rounding precision for the input of the SDP solver (default value = 10)
- `gmp`: a boolean variable for the use of arbitrary-precision SDPA-GMP solver (default value = false)
- `algo`: an integer which is equal to 1 for multivsos and 2 for a previous algorithm developed by Parrilo and Peyrl in https://www.sciencedirect.com/science/article/pii/S0304397508006452 (default value = 1)

Let us consider the polynomial f := x^4 + x^3 y - 7/4 x^2 y^2 - 1/2 x y^3 + 5/2 y^4. 

To compute a sums of squares decomposition of f, you can type:

`f := x^4 + x^3*y - 7/4*x^2*y^2 - 1/2*x*y^3 + 5/2*y^4: out:=multivsos_internal(f,epsilon=2,precSVD=30,precOut=2,precIn=10); sos := out[4]: rlist:=out[3]: sos;`

                                                                                                         2
                      2      2             395    2      2                  2                    2      y
        [1/12, x y - y , 0, x , 5/36, x y, ----, y , 1, x  + 1/2 x y - 4/3 y , 1, 2/3 x y + 3/4 y , 1, ----]
                                           7056                                                         7

`rlist;`
                                                      [7]


The first output sos is a list [c<sub>1</sub>,p<sub>1</sub>,...,c<sub>r</sub>,p<sub>r</sub>], where each c<sub>i</sub> is a rational number and each p<sub>i</sub> is a rational polynomial such that f admits the following weigthed SOS decomposition:

f  = c<sub>1</sub> p<sub>1</sub> <sup>2</sup> + ... + c<sub>r</sub> p<sub>r</sub> <sup>2</sup>

The second output rlist provides a list (with a single element in the unconstrained case) indicating the number of squares in the decomposition.

You can verify afterwards that this yields a valid nonnegativty certificate of f with the following command:

`s := 0: for i from 1 to nops(sos)/2 do s := s + sos[2*i-1]*sos[2*i]^2 od: expand (f -s);`

You can also directly use the main procedure, which will call `multivsos_internal` recursively:

`multivsos(f);`

To avoid recursive calls, you can set the corresponding global parameter as follows in the `multivsos/multivsos.mm` file:

`$define increase_precision_rec false`

To avoid checking positivity of the perturbed polynomial, you can set the corresponding global parameter as follows in the `multivsos/multivsos.mm` file:

`$define positivity_check false`


#### Constrained problems

In the constrained case, the `multivsos_internal` procedure takes as input the same parameters as in the unconstrained case as well as:

- `glist`: the list of polynomial [g<sub>1</sub>,...,g<sub>m</sub>] encoding the set of inequality constraints g<sub>1</sub>(x) >= 0,...,g<sub>m</sub>(x) >= 0. For instance, the hypercube [-1,1]^2 is encoded by `[1 - x^2, 1 - y^2]` (default value = [])
- `relaxorder`: an integer for the maximal degree of the SOS decomposition (default value = 0)

Let us consider the polynomial f := -x^2  - 2 x y - 2 y^2 + 6 on the hypercube encoded by the list of two polynomial constraints g :=  [1 - x^2, 1 - y^2]. To compute an SOS decomposition with maximal degree of 2, you can execute the following commands:

`f:=  -x^2  - 2 * x * y - 2 * y^2  + 6: g :=  [1 - x^2, 1 - y^2]: out := multivsos_internal(f,epsilon=1,precSVD=10,precOut=3,100,glist=g,relaxorder=1): rlist = out[3]: sos:=out[4]:`


                   23853407      23     130657269                              y
    sos, rlist := [---------, 1, --, x, ---------, y, 1, 1/2442, 1, x - y, 1, ----, 1, 11/7, 1, 13/7], [6, 1, 1]
                   292204836     49     291009481                             2437

The first output is a list [c<sub>0 1 </sub>, p<sub>0 1 </sub>,...,c<sub>0 r <sub>0</sub> </sub>, p<sub>0 r <sub>0</sub> </sub>,...,c<sub>m 1 </sub>, p<sub>m 1 </sub>, ...c<sub>m r <sub>m</sub> </sub>, p<sub>m r <sub>m</sub> </sub>] where each  c<sub>i j </sub> is a rational number and each p<sub>i j </sub> is a rational polynomial such that f admits the following weigthed SOS decomposition:

f  = c<sub>0 1 </sub> p  <sub>0 1 </sub> <sup>2 </sup> +...+c<sub>0 r <sub>0</sub> </sub>, p<sub>0 r <sub>0</sub> </sub> <sup>2 </sup> + g <sub> 1 </sub> [ c<sub>1 1 </sub> p<sub>1 1 </sub> <sup>2 </sup> + ... + c<sub>1 r <sub>1</sub> </sub> p<sub>1 r <sub>1</sub> </sub> <sup>2 </sup> ] +...+ g <sub> m </sub> [c<sub>m 1 </sub> p<sub>m 1 </sub> <sup>2 </sup> + ... + c<sub>m r <sub>m</sub> </sub> p<sub>m r <sub>m</sub> </sub> <sup>2 </sup>].


The second output is the list [r<sub>0 </sub>,...,r<sub>m</sub>].

You can verify afterwards that this yields a valid nonnegativty certificate of f with the following commands:

`oneg := [1,op(g)]:s := 0: idx:=0: for i from 1 to nops(g)+1 do idxi:=idx+rlist[i]:for j from idx+1 to idxi do s := s + oneg[i]*sos[2*j-1]*sos[2*j]^2: od: idx:=idxi: od: expand (f - s);`

You can also directly use the main procedure:

`multivsos(f,g);`

#### Benchmarks

See the file `allbenchs.mm` 
