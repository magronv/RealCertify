read "realcertify.mm":

test:=proc(file)
  local failures, successes, bitsize, t, i, b:
  read file:
  failures := 0:
  successes := 0:
  bitsize := 0:
  t := 0:
  for i from 1 to nops(F) do
    b:= multivsos(F[i]):
    if nops(b)=1 then
      failures := failures + 1:
    else
      successes := successes + 1:
      bitsize := b[1]+bitsize: t := b[2]+t:
    fi: 
  od;
  if successes = 0 then return 0; 
  else
  return 2*successes,1.0*(bitsize/successes), t/successes:
  fi:
end:

