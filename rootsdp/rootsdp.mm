with(combinat):with(ListTools): with(ArrayTools): with(LinearAlgebra):with(convex):

$define tolrank 1e-1


monshom := proc(n,d)
  local l1;
  l1 := [seq(1,i=1..n)];
  return map(_c -> _c - l1, combinat:-composition(n+d, n)):
end:

# For both revmons functions, the bottlneck is the composition function from package combinat

revmons := proc(n,d) 
  if d = 0 then 
  return [[seq(0,i=1..n)]]:
  else return [op(monshom(n,d)), op(revmons(n,d-1))]:
  fi:
end:

revmons2 := proc(n,d) 
  local mons0;
  mons0 := [[seq(0,i=1..n)]]:
  return [seq(op(monshom(n,d-i+1)),i=1..d),mons0]:
end:

#Calcule les exposants de l'ensemble des monomes de degre <=d en n variables
monspt := proc(n,d)
  return Reverse(revmons(n,d)):
end:

mymons:=proc(vars,d)
local ms;
if d=1 then 
   return [1, op(vars)]:
else
    ms:=procname(vars,d-1):
    return [op({op(ms),
                seq(seq(v*m, v in vars), m in ms)})]:
fi:
end:

mons := proc(n,d,X,ms::list := monspt(n,d))
  return map(a->mul(X[i]^a[i],i=1..n),ms):
end:

support := proc (f,X,monslist)
  local cf, idxf,monsf;
  cf := coeffs(expand(f),X,'monsf');
  monsf := [monsf];
  idxf := seq(ListTools:-Search(m,monslist),m in monsf);
  return [cf],[idxf];
end:

dense_perturbation_even := proc(ms)
  return add(m^2,m in ms);
end:

dense_perturbation := proc(ms)
  return add(m,m in ms);
end:

absf := proc(fc,fidx,ms)
  return sum(abs(fc[i])*ms[fidx[i]],i=1..nops(fc)):
end:

sos2sdp := proc(r,X,rmin,relaxorder,
                mspt,msptk,ms,msk,
                sparse_mspt,sparse_msptk,sparse_ms,sparse_msk,
                rc,ridx,nsdpg,
                precSVD::integer:=10,
                precSDP::integer:=200,
                epsStar::integer:=3,
                precRound::integer:=30,
                dataprec::integer:=10,
                gmp::boolean:=false,
                algo::integer:=1,
                g::list:=[],
                gc::list:=[],
                gidx::list:=[],
                sparseNP::boolean:=false)
local start_idx, sub_idx,nvars,zero,d,k,n,ng,r0,msdp,nsdp,nblock,fd,rList,i,rfloat,j,lowerbnd,eigs,eivs,eigs2,eivs2,Y, Yrat, Xmat,Xrat,KerX, absorb_tbl,m, absorb_PP,msdpg,nsdponeg,gic,giidx,ni,ig,igc,gifloat,mgi,eigsi,eivsi,rroots,epsList,alg,mski;

   #MS: Ca c'est vraiment bizarre
   d = 2*relaxorder:
   n := nops(X);
   ng := nops(g):
   zero := [seq(0,i=1..n)]:


   msdp := nops(sparse_mspt); nsdp := nops(sparse_msk); nblock := 1+ng: 

   fd := fopen("in.dat-s",WRITE,TEXT); 
   rList := Array([seq(0,i=1..msdp)]):
   
   for i from 1 to nops(rc) do 
    rList[ridx[i]]:=rc[i]: 
   od:

      if sparse_mspt[1] = zero then
         nvars := msdp - 1:
         start_idx := 2:
         r0 := rList[1]: 
      else
          sub_idx := ListTools:-Search(2*sparse_mspt[1],sparse_mspt)-1:
          nvars := msdp - sub_idx:
          start_idx := 1:
          r0 := 0:
      fi: # test whether 0 belongs the multi-index list
          
      writeline(fd,convert(nvars,string)); # Number of moment variables - 1 (since y0 = 1)
      writeline(fd,convert(nblock,string)); # Number of blocks: 1 for the moment matrix in the unconstrained case
      fprintf(fd,"%d ",nsdp): # Size of the first block: the moment matrix has size nsdp

      
      for ni in nsdpg do fprintf(fd,"%d ",ni): od:
      # Size of the localizing matrices: for a polynomial gi of degree di, the size is binomial(n+relaxorder-ceil(di/2),n)
      fprintf(fd,"\n");

      rfloat := convert(rList,float,dataprec):

  if sparse_mspt[1] = zero then 
    for i from start_idx to msdp do fprintf(fd,"%s ",convert(rfloat[i],string));od;
  else
    for i from sub_idx + 1 to msdp do fprintf(fd,"%s ",convert(rfloat[i],string));od;
  fi:
  fprintf(fd,"\n");

### The case when y0 = 1 because 0 belongs to the multi-index list
# the two following for loops may be merged
# starts with the first row of the moment matrix
  if sparse_mspt[1] = zero then 
  writeline(fd,"0 1 1 1 -1"): # Moment variable y0 = 1
  for j from 2 to nsdp do
    k := ListTools:-Search(sparse_msptk[j],sparse_mspt):
    fprintf(fd, "%d 1 %d %d 1\n", j-1, 1, j); # Moment variable y_{j-1} at first row and column j
  od;
# then the other rows of the moment matrix
  for i from 2 to nsdp do
    for j from i to nsdp do
      k := ListTools:-Search(sparse_msptk[i]+sparse_msptk[j],sparse_mspt):
      fprintf(fd, "%d 1 %d %d 1\n", k-1, i, j); # Moment variable y_{k-1} at row i and column j
    od:
  od:
# then the localizing matrices
for ig from 1 to ng do
   gic := gc[ig]: giidx := gidx[ig]: 
   for igc from 1 to nops(gic) do
     mgi := mspt[giidx[igc]]:
     #lprint(mgi);
     gifloat := convert(gic[igc],float,dataprec):
     if mgi = zero then 
      fprintf(fd, "0 %d 1 1 %s\n", ig+1,convert(-gifloat,string)); # Moment variable y_{j-1} at first row and column j
      for j from 2 to nsdpg[ig] do
        fprintf(fd, "%d %d 1 %d %s\n", j-1, ig+1, j, convert(gifloat,string)); # Moment variable y_{j-1} at first row and column j
      od;
     for i from 2 to nsdpg[ig] do
       for j from i to nsdpg[ig] do
         k := ListTools:-Search(mspt[i]+mspt[j]+mgi,sparse_mspt):
         fprintf(fd, "%d %d %d %d %s\n",k-1, ig+1, i, j, convert(gifloat,string)); # Moment variable y_{k-1} at row i and column j
       od:
     od: 
     else
     for i from 1 to nsdpg[ig] do
       for j from i to nsdpg[ig] do
         k := ListTools:-Search(mspt[i]+mspt[j]+mgi,sparse_mspt):
         fprintf(fd, "%d %d %d %d %s\n",k-1, ig+1, i, j, convert(gifloat,string)); # Moment variable y_{k-1} at row i and column j
       od:
     od: 
     fi:
   od:
od:

### The case when y0 = 1 because 0 does not belong to the multi-index list
# there is a bug in sdpa-gmp when not subtracting the "useless" moment variables with k - sub_idx
# This case should happen only in the unconstrained case, therefore we fill the moment matrix entries only
  else
  for i from 1 to nsdp do
    for j from i to nsdp do
      k := ListTools:-Search(sparse_msptk[i]+sparse_msptk[j],sparse_mspt):
      fprintf(fd, "%d 1 %d %d 1\n", k-sub_idx, i, j); # Moment variable y_{k-sub_idx} at row i and column j
    od;
  od;
  fi:

#### Loop again to find the absorbers (this loop can be saved thanks to the previous one)
  absorb_tbl := table([seq(0,i=1..msdp)]):
  for i from 1 to nsdp do
    for j from i to nsdp do
      m := sparse_msptk[i]+sparse_msptk[j]:
      if not_even(m) then k:= ListTools:-Search(m,sparse_mspt): absorb_tbl[k]:=i: fi:
    od:
  od:
  # for i from 1 to msdp do lprint(absorb_tbl[i]): od:

#### In this table, for each gamma (appearing at index k in mspt), one indicates all monomials alpha (and implicitely beta) such that gamma = alpha + beta 
  absorb_PP := table([seq([],i=1..msdp)]):
  for i from 1 to nsdp do
  k:= ListTools:-Search(2*sparse_msptk[i],sparse_mspt): absorb_PP[k]:=[op(absorb_PP[k]),i,i]:
    for j from i+1 to nsdp do
      m := sparse_msptk[i]+sparse_msptk[j]: k:= ListTools:-Search(m,sparse_mspt): absorb_PP[k]:=[op(absorb_PP[k]),i,j,j,i]:
    od:
  od:
  # lprint(msdp); lprint(mspt); 
  # for i from 1 to msdp do lprint(absorb_PP[i]); od:

#### old version, works only when all monomials belong to the NP
#  for j from 2 to nsdp do
#    fprintf(fd, "%d 1 %d %d 1\n", j-1, 1, j); # Moment variable y_{j-1} at first row and column j
#  od;
#  for i from 2 to nsdp do
#    for j from i to nsdp do
#      k := Search(mspt[i]+mspt[j],mspt):
#      fprintf(fd, "%d 1 %d %d 1\n", k-1, i, j); # Moment variable y_{k-1} at row i and column j
#    od;
#  od;

  fclose(fd);

  # SED commands perform as in univsos/univsos3.mm
  system("sed -i 's/ \\./ 0\\./g' in.dat-s"); system("sed -i 's/^\\./0\\./g' in.dat-s"); system("sed -i 's/^-\\./-0\\./g' in.dat-s"); system("sed -i 's/ -\\./ -0\\./g' in.dat-s");
  system("rm -f out.dat-s"); system("rm -f out.mm");
  if not gmp then
    lprint ("double precision"):
    system("sdpa -ds in.dat-s -o out.dat-s -p param.sdpa > /dev/null");
  else
    write_param(precSDP,epsStar,epsStar);
    system("sdpa_gmp -ds in.dat-s -o out.dat-s -p my_param_gmp.sdpa > /dev/null");
  fi:

  # the GREP/SED commands looks for the values of objValPrimal, xMat and yMat

  system("echo $(grep objValDual out.dat-s) ';' 'xMat:=' $(sed -n '/xMat /,/yMat/{//!p}' out.dat-s) ';' 'yMat:=' $(sed -n '/yMat/,/main/{//!p}' out.dat-s) ';' >> out.mm");
  system("sed -i 's/ =/ :=/g' out.mm"): 
  system("sed -i 's/{/[/g' out.mm"): system("sed -i 's/}/]/g' out.mm"); system("sed -i 's/] \\[/], \\[/g' out.mm");
  read "out.mm": 
  # OLD ABSORPTION if algo = 1 then lowerbnd := r0 + convert(objValDual,rational,exact) : else lowerbnd := 0: fi:
  lowerbnd := r0 + convert(objValDual,rational,exact) :
  printf("lower bound: "); lprint(evalf(lowerbnd));
  eigs:=Array([]); eivs := Array([]);
  nsdponeg:=[nsdp,op(nsdpg)]:
  Xmat := Matrix(xMat[1]); 

  # TODO reimplement rroots with sparse_mspt
  if sparseNP then 
    rroots := []: 
    #rroots := extract_realroots(Xmat,n,rmin,relaxorder,sparse_mspt):
  else rroots := extract_realroots(Xmat,n,rmin,relaxorder,sparse_mspt): fi:

  alg := 1:
  for j from 1 to ng+1 do # handles all dual SDP matrices except the first one corresponding to sigma_0
    i := ng+2-j:
    Y := Matrix(yMat[i]): 
    if i > 1 then mski := msk[1..nsdponeg[i]]: else mski := sparse_msk: fi:
    if algo = 1 or i > 1 then Yrat:=Matrix(Y): 
    else # case when algo = 2 and i = 1
     if precRound >= 30 then Y := convert(Y,rational,exact): else Y := convert(Y,rational,precRound) fi:
     Y := (Y + Transpose(Y))/2;
     epsList := epsPP(convert(eigs,list),convert(eivs,list),r,g,nsdpg,X,sparse_ms,msdp);
     Yrat := absorber_PP(Y,epsList,absorb_PP,nsdp,sparse_msptk, sparse_mspt):
     #printf("err="); lprint(expand(Transpose(Vector(msk)).Yrat.Vector(msk)+lowerbnd-r)) 
    fi:

    if algo = 2 and i = 1 then alg := 2: fi:
    (eigsi,eivsi) := eigseivs(Yrat,X,mski,precSVD,precRound,alg);
    eigs:=Concatenate(1,eigsi,eigs):  eivs:=Concatenate(1,eivsi,eivs):
   od:
  return convert(eigs,list), convert(eivs,list),lowerbnd,absorb_tbl,nsdponeg,rroots:
end;

epsPP := proc(eigs,eivs,r,g,nsdpg,X,ms,msdp)
  local idx,i,sumsos,idxi,epsList,epsc,epsidx;
  idx := 0: 
  #sumsos := -1:
  sumsos := r - 2: # to retrieve -1 when r = 1
  for i from 1 to nops(g) do
    idxi := idx + nsdpg[i]:
    sumsos := sumsos - g[i]*sum(eigs[j]*eivs[j]^2,j=idx+1..idxi);
    idx := idxi:
  od:
  epsc,epsidx := support(sumsos,X,ms):
  epsList := Array([seq(0,i=1..msdp)]):
  for i from 1 to nops(epsc) do epsList[epsidx[i]]:=epsc[i]: od:
  return epsList:
end:


absorber_PP := proc(Yrat,rList,absorb_PP,nsdp,msptk,mspt)
  local Yproj,i,j,i1,k,m,list_m,nopsm,maxY,minY,yij;
  # for i from 1 to nsdp do
  #  for j from i to nsdp do 
  #    yij := Yrat[i,j]: if abs(yij) < 1/10^3 then Yrat[i,j] := 0: Yrat[j,i]:=0: fi:
  #  od:
  #od:
   #Yproj := Matrix(Yrat,shape='symmetric');
   Yproj := Matrix(nsdp,nsdp,shape='symmetric');
   maxY := max(max(abs(Yproj))):    minY := min(min(abs(Yproj))): 
   #lprint(evalf(minY)); lprint(evalf(maxY)); #lprint(Y); #lprint(rList);
   for i from 1 to nsdp do
    for j from i to nsdp do 
      m := msptk[i] + msptk[j]; k:= ListTools:-Search(m,mspt):
      list_m := [op(absorb_PP[k])]: nopsm := nops(list_m)/2:
      Yproj[i,j] := Yrat[i,j] - 1/nopsm*(add(Yrat[list_m[2*im-1],list_m[2*im]],im=1..nopsm) - rList[k]):
    od:
  od:
  return Yproj: 
end:




## numerical rank, up to relative tolerance given by tolrank
numrank := proc(M,n,s)
   local V,S,VT,DS,r,card_ns,Ms,rk;
   card_ns := binomial(n+s,n);
   if card_ns = 1 then r := 1: V := M: DS:= Matrix(1,1,1): else
    Ms := M[1..card_ns,1..card_ns];
    (V,S,VT) := MTM[svd](Ms):
    DS := Diagonal(S);
    DS := DS +~1e-15;
    rk := SelectFirst(_s -> _s < tolrank, DS[2..card_ns]/~DS[1..card_ns-1],output='indices');
    if rk = NULL then r := card_ns: else r := rk: fi:
   fi:
   return [r, Multiply(V[1..card_ns,1..r],DiagonalMatrix(map(sqrt,DS[1..r])))];
end:

split_list := proc (l)
  return map(_l->_l[1],l), map(_l->_l[2],l):
end;

## rank condition by Curto and Fialkow
rankCF := proc(M,n,d,k)
  local deg,idxr,ranks_Vs,ranks, Vs, V, rCF;
  ranks_Vs := [seq(numrank(M,n,deg),deg=0..k)]: # list of size k+1
  ranks, Vs := split_list(ranks_Vs):
  printf("ranks of submatrices = "); lprint(ranks);
  idxr := 2: 
  while idxr <= k do
    if ranks[idxr] = ranks[idxr+1] then 
    rCF := ranks[idxr+1]: V := Vs[idxr+1]: break: fi:
    idxr := idxr+1:
  od:
  if idxr = k+1 then rCF := 0; V := Matrix(1,1,0):fi:
  return rCF,V:
end:

multiplication_matrix := proc(U,mspt,i,n,r)
  local Ni,msptr,deltai,mspti,j,k;
  Ni := Matrix(r,r,0);
  msptr := mspt[1..r];
  deltai := [seq(0,j=1..n)]; deltai[i] := 1;
  mspti := map(m->m+deltai,msptr);
  for j from 1 to r do
    k := ListTools:-Search(mspti[j],mspt):
    Ni[j]:=U[k]:
  od:
  return Ni;
end:

roundU := proc(U)
  local r,s,i,j,roundedU;
  r := LinearAlgebra:-ColumnDimension(U):
  s := LinearAlgebra:-RowDimension(U):
  roundedU := Matrix(s,r,0); 
  for i from 1 to s do
   for j from 1 to r do
     if abs(U[i,j]) > 1e-6 then roundedU[i,j]:=U[i,j]: fi: 
   od:
  od:
  return roundedU:
end:


roll01 := proc() 
  return RandomTools:-Generate(float('range' = 0. .. 1.0, 'method' = 'uniform'));
end;

random_weights := proc(n)
  local l;
  l := [seq(roll01(),i=1..n)];
  return l/add(k,k in l);
end;

# Givens rotation for ordered Schur decomposition
givens := proc(a,b)
  local c,s,t;
  if b = 0 then
    c := 1: s := 0:
  else
    if abs(b) > abs(a) then
      t := -a/b: s := 1/sqrt(1+t^2): c := s*t:
    else
      t := -b/a: c := 1/sqrt(1+t^2): s := c*t:
    fi:
  fi:
  return c,s
end:

orderSchur := proc(N,r)
  local QN,TN,Q,c,s,k,ord,nit,Msc;
  TN, QN := SchurForm(N,output=['T','Z']);
  if norm(Im(Diagonal(TN))) > 1e-8 # should be real
    then TN := [];
  else
    QN := Re(QN); TN := Im(TN);
    ord := false; nit := 0;
    while not ord do # while the order is not correct
      ord := true;
      for k from 1 to r-1 do
        if TN[k,k] - TN[k+1,k+1] > 1e-8 then
          ord := false; # diagonal elements to swap
          # Givens rotation
          c,s := givens(TN[k,k+1],TN[k+1,k+1]-TN[k,k]);
          Msc := Matrix([[c, s],[ -s, c]]);
          TN[k..k+1,k..r] := Msc^%T.TN[k..k+1,k..r];
          TN[1..k+1,k..k+1] := TN[1..k+1,k..k+1].Msc;
          QN[1..r,k..k+1] := QN[1..r,k..k+1].Msc;
        fi:
      od:
      nit := nit+1;
     od:
  fi:
  return QN:
end:

extract_realroots := proc(Xmat, n, d, k, mspt)
  local rroots,U,V,roundedV,r,Nlist,i,j,randw,N,Q,T,Ni,qj,Qtr;
  r,V := rankCF(Xmat,n,d,k); 
  roundedV := roundU(V);
  rroots := Matrix(n,r,0);
  if r > 0 then 
  U := (MTM[rref](roundedV^%T))^%T; 
  Nlist := [];
  for i from 1 to n do
    Nlist := [op(Nlist),multiplication_matrix(U,mspt,i,n,r)];
  od:
  randw := random_weights(n);
  N := sum(randw[j]*Nlist[j],j=1..n):
  Q := orderSchur(N,r);
  Qtr := Q^%T;
  # lprint(Q);
  for i from 1 to n do
    Ni := Nlist[i];
    for j from 1 to r do
      qj := Qtr[j]; 
      rroots[i,j] := qj.Ni.qj^%T:
    od: 
  od: 
  fi:
  return rroots;
end;

write_param := proc(precSDP,epsStar,epsDash)
  local fd;
  fd := fopen("my_param_gmp.sdpa",WRITE,TEXT); 
  fprintf(fd,"10000	unsigned int maxIteration;\n");
  fprintf(fd,"1.0E-%d	double 0.0 < epsilonStar;\n",epsStar);
  fprintf(fd,"1.0E4   double 0.0 < lambdaStar;\n");
  fprintf(fd,"2.0   	double 1.0 < omegaStar;\n");
  fprintf(fd,"-1.0E10  double lowerBound;\n");
  fprintf(fd,"1.0E10   double upperBound;\n");
  fprintf(fd,"0.1     double 0.0 <= betaStar <  1.0;\n");
  fprintf(fd,"0.3     double 0.0 <= betaBar  <  1.0, betaStar <= betaBar;\n");
  fprintf(fd,"0.9     double 0.0 < gammaStar  <  1.0;\n");
  fprintf(fd,"1.0E-%d	double 0.0 < epsilonDash;\n",epsDash);
  fprintf(fd,"%d     precision\n",precSDP);
  fprintf(fd,"%%+10.30Fe     char* \t xPrint \t   (default %%+8.3e, \t    NOPRINT skips printout) \n");
  fprintf(fd,"%%+10.30Fe     char* \t XPrint \t   (default %%+8.3e, \t    NOPRINT skips printout) \n");
  fprintf(fd,"%%+10.30Fe     char* \t YPrint \t   (default %%+8.3e, \t    NOPRINT skips printout) \n");
  fprintf(fd,"%%+10.30Fe     char* \t infPrint \t (default %%+10.16e, \t  NOPRINT skips printout) \n");
  fclose(fd);
end;

checkrational := proc(U)
  local v:
  for v in U do:
    if not type(convert(v,rational),realcons) then 
    lprint(v): 
    error "Non Rational Cholesky factor, retry with gmp = true or increase the precision":fi:
  od:
  return:
end;

absorber := proc(u,X,e,even_mons,msptk,ms,mspt,absorb_tbl)
  local i,j,k,ucoeffs,uidx,uc,err_list,err,m,bad_m,n,c,cm,cp,cfs,sqs,n1,m1,m2,k1,k2;
  #printf("\nu = "); lprint(evalf(u));
  ucoeffs,uidx := support(u,X,ms);
  n := nops(X): 
  #printf("\neven_mons = "); lprint(even_mons);
  err_list := Array([seq(e,i=1..nops(even_mons))]); 
  cfs := []; sqs := [];
  for i from 1 to nops(ucoeffs) do
    uc := ucoeffs[i]; bad_m := mspt[uidx[i]]:
    if not_even(bad_m) then
      k := ListTools:-Search(bad_m,mspt):m1:=msptk[absorb_tbl[k]]:m2:=bad_m - m1;
      #printf("\n"); lprint(bad_m); lprint(m1); lprint(m2);
      cfs := [op(cfs),abs(uc)/2]; 
      sqs := [op(sqs),mul(X[i]^m1[i],i=1..n)+sign(uc)*mul(X[i]^m2[i],i=1..n)]:
      k1:=ListTools:-Search(2*m1,even_mons): k2:=ListTools:-Search(2*m2,even_mons):
      err_list[k1] := err_list[k1]-1/2*abs(uc): err_list[k2] := err_list[k2]-1/2*abs(uc):

    else 
      k := ListTools:-Search(bad_m,even_mons): err_list[k]:=err_list[k]+uc:
    fi:
  od:
  err_list := convert(err_list,list);
  # printf("\nerr list = "); lprint(evalf(err_list));
  cfs := [op(cfs),op(err_list)]; sqs := [op(sqs),seq(mul(X[i]^(m[i]/2),i=1..n), m in even_mons)];
  return cfs,sqs:
end;



eigseivs := proc(Yrat,X,ms,precSVD,precRound,algo)
  local v, e, msvec, eigs, eivs, U,S,V, Ysvd,SVD,ti,tcmp;
  SVD := false:
  msvec := Vector(ms);
  # if SVD then 
  #  Digits:=precSVD;
  #  lprint("starting SVD");
  #  ti := time():
  #  (U,S,V) := MTM[svd](Yrat); 
  #  tcmp := time() - ti: lprint (tcmp);
  #  lprint("ending SVD");
  #else
    ti := time():
    # lprint("starting Cholesky");
    if algo = 1 then Digits := precSVD:fi:
    U := LUDecomposition(Yrat,method='Cholesky');
    # lprint(Eigenvalues(Yrat));
    # lprint(Eigenvectors(Yrat));

    checkrational(U):
    tcmp := time() - ti:
    # lprint (tcmp);
    # lprint("ending Cholesky");
    S := IdentityMatrix(nops(ms));
  # fi:
    eigs := Diagonal(S); eivs := Transpose(Transpose(msvec).U);
    Digits:=10;
    Ysvd := U.S.V^%T;
    #lprint(Ysvd); #lprint(max(max(Y),-min(Y))); #lprint(max(max(Ysvd),-min(Ysvd))); #lprint(max(Y - Ysvd));
    if precRound >= 30 then 
      # lprint("exact");  
      eigs := convert(eigs,rational,exact); eivs := map (_e -> convert(_e,rational,exact), eivs):
    else eigs := convert(eigs,rational,precRound); eivs := map (_e -> convert(_e,rational,precRound), eivs):
    fi:
    return (eigs, eivs):
end;

not_even := proc(m)
  return has(1,map(_c -> irem(_c,2), m)):
end;


truncate_withNP := proc(f,X,mspt,msptk,ms,use_convex)
  local NP,msptkNP,msptNP,m,i;
  if use_convex then 
    NP := newtonpolytope(f, X): 
  else NP:=NP_PolyhedralSets(f,X,mspt,ms): fi:
  #msptkNP := [];
  #for m in msptk do if contains(NP,2*m) then msptkNP := [op(msptkNP),m] : fi: od:
  if use_convex then
    msptkNP:=map(m->if contains(NP,2*m) then m fi, msptk):
  else msptkNP:=map(m->if 2*m in NP then m fi, msptk): fi:

  if use_convex then
    msptNP:=map(m->if contains(NP,m) then m fi, mspt):
  else msptNP:=map(m->if m in NP then m fi, mspt): fi:

 #   wrong code !
 (** msptNP := [op(msptkNP)]:
   if use_convex then 
   for i from nops(msptk) + 1 to nops(mspt) do
      m := mspt[i]:
      if contains(NP,m) then msptNP := [op(msptNP),m] : fi:
    od:
  else
    for i from nops(msptk) + 1 to nops(mspt) do
      m := mspt[i]:
      if m in NP then msptNP := [op(msptNP),m] : fi:
    od:
  fi:
  **)
  return msptNP,msptkNP:
end:

testNP := proc(f,X,use_convex)
  local d,k,n,card_nk,mspt,msptk,ms;
  d := degree(f): k := d/2: n := nops(X): card_nk := binomial(n+k,k):
  mspt := monspt(n,d): msptk := mspt[1..card_nk]: ms := mons(n,d,X,mspt):
  return truncate_withNP(f,X,mspt,msptk,ms,use_convex):
end:


relaxordermin := proc(g)
 return ceil(max(seq(degree(gi),gi in g))/2):
end:

locmatsizes := proc(g,k,n)
  return [seq(binomial(n + k - ceil(degree(gi)/2),n),gi in g)],[seq(binomial(n + 2* k - 2*ceil(degree(gi)/2),n),gi in g)]:
end;

maxcoeff := proc(f)
  return max(map(_c -> abs(_c),[coeffs(expand(f))]));
end:


soscheck1:=proc(f, sos, nsdponeg, g::list:=[])
  local s,i,j,idx,idxi,oneg;
  oneg := [1,op(g)]:
  s := 0; idx:=0;
for i from 1 to nops(g)+1 do
  idxi:=idx+nsdponeg[i]:
  for j from idx+1 to idxi do 
      if sos[2*j-1] < 0 then
         lprint(evalf(sos[2*j-1])):
         error "Negative number => invalid sum of squares decomposition"; 
      else
          s := s + oneg[i]*sos[2*j-1]*sos[2*j]^2
      fi: 
  od:
  idx:=idxi:
  od:

  #MS: Il ne faut surtout pas faire comme ca pour des raisons d'efficacite.
  #Proceder par evaluation
  if not expand(f - s) = 0 then lprint(evalf(expand(f - s))); error "Inexact sum of squares decomposition"; else return 0: fi:
end;

BitSizeSos := proc (sos,X)
   return add(BitSizePol(p[1], X) + BitSizePolQuadr(p[2], X), p in sos);
end;

BitSizePolQuadr := proc(q,X)
  return BitRat(q[1]) + BitSizePol(q[2], X) + BitRat(q[3]);
end;

BitSizePolSeq3 := proc(listpol,X)
  return add(BitSizePol(p[1], X) + BitSizePolSeq(p[2], X), p in listpol);
end;

BitSizePolSeq := proc(listpol,X)
  return add(BitSizePol(p, X), p in listpol);
end;

BitSizePol:=proc(p, X)
  local res;
  res := [coeffs(expand(p),X)];
  return BitSizeSeq(res);
end;

BitSizeSeq:=proc(l)
  return add(BitRat(c), c in l);
end;

BitRat := proc(r)
  local n, d, res,rs;
  if type(r,rational) then rs :=r : else rs:=r^2: fi:
  if rs = 0 then return 1; fi;
  (n, d) := (abs(numer(rs)), abs(denom(rs)));
  if d = 1 then res :=  ilog2(n) + 1 else res := ilog2(n) + ilog2(d) + 2 fi;
  return res;
end;

sparse_sigma := proc(f,g,gc,gidx,msdpg,ms,X,mspt,msptk,use_convex)
  local sigma0,i,gi;
  sigma0 := f:
  for i from 1 to nops(g) do
    gi := g[i]:
    sigma0 := sigma0 + absf(gc[i],gidx[i],ms)*add(m, m in ms[1..msdpg[i]]):
  od:
  return truncate_withNP(sigma0,X,mspt,msptk,ms,use_convex):
end:


rootsdp:=proc(f,
              prec::integer := 10,
              precSVD::integer := 10,
              precSDP::integer := 200,
              epsStar::integer := 3,
              precRound::integer:=30,
              dataprec::integer:=10,
              gmp::boolean:=false,
              glist::list:=[],
              relaxorder::integer:=0)
local p,d,mspt,ms,rc,ridx,ng,gc,gidx,gic,giidx,S,s,c, q,n,k,t,e,r,l,a,s1,s2,u,v,i,j,sqs,cfs,sos,rfloat, eigs, eigsg, eigsgi, soslist,soslistg, soslistgi, sumsos,cnd,maxq,obj_plus_r0, card_nk, even_mons,err_list,err,msptk,msk,absorb_tbl,rmin,nsdponeg,idx,oneg,idxi,g,X,ti,tf,gi,cg,use_convex,algo,rroots,diff_pol,eps0,epsM,kepsM,eps1,radius_unfeas,critical_points,nsdpg,msdpg,sparse_mspt,sparse_msptk,sparse_ms,sparse_msk,sparseNP;


  algo := 1: critical_points := false: sparseNP := false:
  use_convex := true: 
  ti := time[real]():
  #c := maxcoeff(f);
  c := 1;
  p := 1/c*f;
  X := [op(indets([p,op(glist)]))]:
  n := nops(X):
  g := [];
  ng := nops(glist);
  for i from 1 to ng do
    gi := expand(glist[i]);
    cg := maxcoeff(gi);
    #cg := max(map(_c -> abs(_c),coeffs(expand(gi)))):
    cg := 1;
    g := [op(g),gi/cg]
  od:
  
  if critical_points then 
  X := [op(X),lambda]:
  for i from 1 to n do
    g := [op(g),expand(lambda*diff(g[1],X[i])-X[i]),-expand(lambda*diff(g[1],X[i])-X[i])]:
  od:
  n := n+1: ng := nops(g):
  fi:

  if lcoeff(p,order=grlex(op(X))) < 0 and ng = 0 then lprint(p): lprint("There is no decomposition into sum of squares for this polynomial"); fi;
  
  rmin := relaxordermin([p,op(g)]): d := 2*max(relaxorder,rmin):
  # if ng = 0 then d := degree(p): else rmin := relaxordermin([p,op(g)]): d := 2*max(relaxorder,rmin): fi:
  k := ceil(d/2);
  if (2 * k <> d and ng = 0) then
     lprint(p): error "There is no decomposition into sum of squares for this polynomial";
  fi;
  if (d = 0 and ng = 0) then lprint(p, " * (", s, ")^2"); fi;

  #Liste des exposants des monomes de degre <=d en n variables
  mspt := monspt(n,d):
  card_nk := binomial(n+k,n): 
  #Liste des exposants des monomes de degre <=k en n variables
  msptk := mspt[1..card_nk];
  ms := mons(n,d,X,mspt):
  msk := mons(n,k,X,msptk):

  gc := []:
  gidx := []:
  for i from 1 to ng do 
    gic,giidx := support(g[i],X,ms):
    gc:=[op(gc),gic]; gidx:=[op(gidx),giidx];
  od:
  nsdpg,msdpg := locmatsizes(g,k,n):

  if sparseNP then 
  sparse_mspt,sparse_msptk := sparse_sigma (p,g,gc,gidx,msdpg,ms,X,mspt,msptk,use_convex): 
  else sparse_mspt := [op(mspt)]: sparse_msptk := [op(msptk)]: fi:
  sparse_ms := mons(n,d,X,sparse_mspt): sparse_msk := mons(n,d,X,sparse_msptk):
   # lprint(sparse_mspt):lprint(sparse_msptk): lprint(sparse_ms):lprint(sparse_msk):

  # let r = p - e * t be a perturbation 
  if algo = 1 then
    t := dense_perturbation_even(sparse_msk);
    # t := dense_perturbation(sparse_ms);
    # printf("\nperturber = "); lprint(t):
    even_mons := 2*sparse_msptk;
    e := 1/2^prec: 
    else e := 0: t := 0: 
  fi:
  e := 0:
  r := expand(p+e*t);
  rc,ridx := support(r,X,sparse_ms):

  (eigs,soslist,obj_plus_r0,absorb_tbl,nsdponeg,rroots) := sos2sdp(r,X,rmin,k,mspt,msptk,ms,msk,sparse_mspt,sparse_msptk,sparse_ms,sparse_msk,rc,ridx,nsdpg,precSVD,precSDP,epsStar,precRound,dataprec,gmp,algo,g,gc,gidx,sparseNP);


  #eigs := e*eigs: obj_plus_r0 := e*(obj_plus_r0-1)+1:

  idx := 0: 
  # OLD ABSORPTION sumsos := obj_plus_r0: 
  sumsos := 0:
  oneg := [1,op(g)];
  for i from 1 to ng+1 do
    idxi := idx + nsdponeg[i]:
    sumsos := sumsos + oneg[i]*sum(eigs[j]*soslist[j]^2,j=idx+1..idxi);
    idx := idxi:
  od:
  radius_unfeas := 0:
  sos := []:
  lprint(evalf(expand(r - sumsos - obj_plus_r0)));

  if obj_plus_r0 > 1 then 
    printf("no real roots\n"): 
    if algo = 1  then
      diff_pol := expand(1+sumsos/(obj_plus_r0 - 1)):
      #lprint(evalf(diff_pol)):
      eps0 := eval(diff_pol,map(i->i=0,X)):
      epsM := maxcoeff(diff_pol);
      #printf("Error polynomial magnitude = %e\n",evalf(epsM));
      kepsM := (binomial(n+d,n) - 1)*epsM: eps1 := abs(1-eps0)/kepsM: 
      radius_unfeas := sqrt(n)*min(eps1,eps1^(1/d)):
      # lprint(eigs); lprint(map(_c -> convert(evalf(_c),rational),soslist));
      printf("Certification radius = %e\n", evalf(radius_unfeas));
      # OLD ABSORPTION u := expand(r - sumsos+obj_plus_r0);
      u := expand(r - sumsos - obj_plus_r0);
      #lprint(soslist); 
      # lprint(evalf(u));
      cfs,sqs := absorber(u,X,e,even_mons,sparse_msptk,sparse_ms,sparse_mspt,absorb_tbl);
      nsdponeg[1]:=nsdponeg[1]+nops(sqs):
    else # algo = 2
      u:=0; cfs := []: sqs := []:
    fi:
    err := expand(u+e*t - add(cfs[i]*sqs[i]^2,i=1..nops(sqs)));
    if algo = 1 then printf("\nerr = "); lprint(evalf(err)); fi:
    cfs := [op(cfs), op(eigs)]: sqs := [op(sqs),op(soslist)]:
    sos := [];
    for i from 1 to nops(sqs) do
      if algo = 1 then sos := [op(sos), c/(obj_plus_r0 - 1)*cfs[i],sqs[i]]: 
      else sos := [op(sos), c*cfs[i],sqs[i]]: 
      fi:
    od;
    # lprint(convert(evalf(cfs),rational));
    if algo = 1 then 
      if soscheck1((f-obj_plus_r0)/(obj_plus_r0-1),sos,nsdponeg,g) = 0 then printf("\n Certified SOS Decomposition with algo = 1\n"): 
        #lprint(evalf(expand (f-obj_plus_r0)/(obj_plus_r0-1))): 
      fi:
    else 
      if soscheck1(f-2,sos,nsdponeg,g) = 0 then printf("\n Certified SOS Decomposition with algo = 2\n"): fi:
    fi:
    printf("bitsize= %d\n",BitSizePolSeq(sos,X));
  fi:

  # lprint(evalf(expand(sumsos + obj_plus_r0)));
  tf := time[real]()-ti; 
  printf("time= %esecs\n",tf);
  return rroots,evalf(radius_unfeas),sos,nsdponeg;
end;

loadpolsys := proc()
  read "polsys.mm":
  return l;
end:

benchsnoroot := proc(n,d,relaxorder,reload::boolean := false)
  local i,X,l,rp,rp1,rp2,cl,exactsol,rw,last_eq;
  if not reload then
  l := [];
  X:=[seq(cat('x',i),i=1..n)]:
  for i from 1 to n-1 do
    rp := randpoly(X,degree=d,dense):
    l := [op(l),rp]:
  od:
  # last_eq := (1-add(x^2,x in X))^(d/2);  
  # rw := map(_r -> convert(_r,rational,exact),random_weights(n)); sphere_eq := sum(l[j]*rw[j],j=1..n-1) + rw[n]*sphere_eq;
  rp1 := randpoly(X,degree=1,dense): rp2 := randpoly(X,degree=1,dense):
  last_eq := rp1^d + rp2^d:
  l := [op(l),last_eq];
  cl:= max(map(h -> maxcoeff(h),l));
  l := l/cl: 
  save l, "polsys.mm";
  else 
   read "polsys.mm": l := loadpolsys():
  fi:  
  #exactsol := HasRealSolutions(map(c -> c = 0, l)); lprint(exactsol);
  return rootsdp(1,30,50,200,30,30,10,true,[op(l),-op(l)],relaxorder);
end:

# rootsdp(1,10,10,200,3,30,10,false,[x1^2+x2^2,-x1^2-x2^2],1); # VR = {[0, 0]}
# rootsdp(x1^2+x2^2,10,10,200,3,30,10,false,[x1^2+x2^2,-x1^2-x2^2],1);

# rootsdp(1,10,10,200,3,30,10,false,[3*x1^2*x2-1,2*x1^3+x2^2-1,-3*x1^2*x2+1,-2*x1^3-x2^2+1],2);
# rootsdp(x1^2+x2^2,10,10,200,3,30,10,false,[3*x1^2*x2-1,2*x1^3+x2^2-1,-3*x1^2*x2+1,-2*x1^3-x2^2+1],2);

# rootsdp(1,10,10,200,3,30,10,false,[x1^2-2*x1*x3+5,x1*x2^2 + x2*x3+1,3*x2^2-8*x1*x3, -x1^2+2*x1*x3-5,-x1*x2^2 - x2*x3-1,-3*x2^2+8*x1*x3],2);
# rootsdp(x1^2+x2^2+x3^2,10,10,200,3,30,10,false,[x1^2-2*x1*x3+5,x1*x2^2 + x2*x3+1,3*x2^2-8*x1*x3, -x1^2+2*x1*x3-5,-x1*x2^2 - x2*x3-1,-3*x2^2+8*x1*x3],2);

# rootsdp(-(x1-1)^2-(x1-x2)^2-(x2-3)^2,10,10,200,3,30,10,true,[1-(x1-1)^2,1-(x1-x2)^2,1-(x2-3)^2],1);

# rootsdp(1,10,10,200,3,30,10,false,[x1^2+x2^2+1,-x1^2-x2^2-1,x1^2+x2^2-1,-x1^2-x2^2+1],1); # VR empty
# rootsdp(1,10,10,200,3,30,10,false,[x1^4+x2^4+x3^4-4,x1^5+x2^5+x3^5-5,x1^6+x2^6+x3^6-6,-x1^4-x2^4-x3^4+4,-x1^5-x2^5-x3^5+5,-x1^6-x2^6-x3^6+6],1); # VR empty
# rho := 2 - 1/16: rootsdp(1,10,10,200,3,30,10,false,[x1*x2-1,1 - x1*x2, x1^2+x2^2 - rho,rho - x1^2 - x2^2],1);
# rootsdp(1,10,10,200,3,30,10,false,[x1*x2-1,1 - x1*x2],1);
# rootsdp(1,10,10,200,30,30,10,false,[x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2, - (x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2)],3); # Motzkin
# rootsdp(1/27+x1^2*x2^2*(x1^2+x2^2-1),0,30,200,30,30,10,false,[],8); # Motzkin with roundoff errors due to the SDP solver (add a perturbation)
# rootsdp(1,0,30,200,30,30,10,true,[x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2, - (x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2),120-x1^2-x2^2],6): # Motzkin on a ball
# rootsdp(1,10,30,200,30,30,20,true,[x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2, - (x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2),300-x1^2-x2^2],5): # Motzkin on a ball with algo 1

# rootsdp(1 + 1/10^2*sum(x1^(2*i)+x2^(2*i),i=1..3),100,50,200,30,30,10,false,[x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2, - (x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2)],3): # algo 2 succeeds for 1/10^2 but fails for 1/10^3
# rootsdp(1 + 1/10^4*sum(x1^(2*i)+x2^(2*i),i=1..3),20,50,200,30,30,10,true,[x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2, - (x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2)],3): # algo 1 succeeds for 1/10^4
# rootsdp(1 + 1/10^30*sum(x1^(2*i)+x2^(2*i),i=1..3),100,50,200,30,30,10,true,[x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2, - (x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2)],3): # algo 1 succeeds for 1/10^30
# rootsdp(1 + 1/10^50*sum(x1^(2*i)+x2^(2*i),i=1..3),200,30,200,100,30,100,true,[x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2, - (x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2)],3): # algo 1 fails : not enough precision for sdp solver
# rootsdp(1 + 1/10^50*sum(x1^(2*i)+x2^(2*i),i=1..3),200,100,1000,100,30,100,true,[x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2, - (x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2)],3): # algo 1 succeeds
# rootsdp(1,10,10,200,30,1,10,true,[x1^4*x2^2 + x1^2*x2^4 -3*x1^2*x2^2 +2, - (x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +2)],5): # success with algo 2 and sparse sigma0 
# rootsdp(1,10,50,300,30,3,30,true,[x1^4*x2^2 + x1^2*x2^4- 3*x1^2*x2^2 +1+1/10, - (x1^4*x2^2 + x1^2*x2^4 - 3*x1^2*x2^2 +1+1/10)],10): # success with algo 2 and sparse sigma0  but needs to increase relaxorder
