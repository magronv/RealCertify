univbench := proc(biteta,biteps,bitin,relax)
  local width,eps,scale,obj1,epsSDP,etaSDP,perturb,l,midl,evs,res,reshigh,gmp,g1:
  width := 20: eps := 1/10^3: 
  # scale := 1: 
  scale := 1/10^4:
  obj1 := scale*(x1-100)^2*((x1-1)^2 + eps/99^2);
  epsSDP :=10^(-biteps): etaSDP := 2^(-biteta):
  g1 := 1/10^2*(x1-1)*(100-x1):
  perturb := obj1 + scale*epsSDP*sum(x1^i,i=0..(2*relax)) + scale*etaSDP*(sum(x1^(2*i),i=0..relax)) + scale*etaSDP*g1*sum(x1^(2*i),i=0..(relax-1)):
  l := realroot(diff(perturb,x1),width):
  midl := map (itv-> (itv[1]+itv[2])/2 ,l):
  evs := map(_x-> eval(perturb,x1=_x),midl):
  if biteta = 64 then gmp := false: else gmp := true: fi:
  res := rootsdp(obj1,10,2000,biteta,biteps,100,bitin,gmp,[],relax):
  # reshigh := rootsdp(perturb,10,2000,1000,100,100,bitin,true,[],relax):
  lprint(obj1);
  return res[1],evalf(midl),evalf(1/scale*evs): #,reshigh[1];
end:

motzkin := proc(biteta,biteps,bitin,relax)
  local obj1, t8u, t8manual, gmp, res, reshigh;
  t8u := 10^(-8)*(x1^16+x1^14*x2^2+x1^12*x2^4+x1^10*x2^6+x1^8*x2^8+x1^6*x2^10+x1^4*x2^12+x1^2*x2^14+x2^16+x1^14+x1^12*x2^2+x1^10*x2^4+x1^8*x2^6+x1^6*x2^8+x1^4*x2^10+x1^2*x2^12+x2^14+x1^12+x1^10*x2^2+x1^8*x2^4+x1^6*x2^6+x1^4*x2^8+x1^2*x2^10+x2^12+x1^10+x1^8*x2^2+x1^6*x2^4+x1^4*x2^6+x1^2*x2^8+x2^10+x1^8+x1^6*x2^2+x1^4*x2^4+x1^2*x2^6+x2^8+x1^6+x1^4*x2^2+x1^2*x2^4+x2^6+x1^4+x1^2*x2^2+x2^4+x1^2+x2^2+1):
   t8manual := convert(-.2632921958e-9*x2^16-.1958078296e-9*x1^16-.1016490197e-9*x1^14*x2^2-.1016604128e-9*x1^12*x2^4-.1016436510e-9*x1^10*x2^6-.1016512059e-9*x1^8*x2^8-.1016781950e-9*x1^6*x2^10-.1016507849e-9*x1^4*x2^12-.1016499649e-9*x1^2*x2^14-.1016586530e-9*x1^2-.1016439570e-9*x2^2-.1027834778e-9*x1^2*x2^2-.1006578376e-9*x1^4*x2^2-.1005658138e-9*x1^2*x2^4-.9856522677e-10*x1^6*x2^2-.1071752216e-9*x1^4*x2^4-.1090923321e-9*x1^2*x2^6-.1032946745e-9*x1^8*x2^2-.9824426127e-10*x1^6*x2^4-.8844748892e-10*x1^4*x2^6-.7859333477e-10*x1^2*x2^8-.9712838754e-10*x1^10*x2^2-.1102405438e-9*x1^8*x2^4-.8741170944e-10*x1^6*x2^6-.8564423184e-10*x1^4*x2^8-.5648714050e-10*x1^2*x2^10-.1015392212e-9*x1^12*x2^2-.1020081026e-9*x1^10*x2^4-.1018157922e-9*x1^8*x2^6-.1020576140e-9*x1^6*x2^8-.1014305694e-9*x1^4*x2^10-.1019633594e-9*x1^2*x2^12-.1016464408e-9*x1^4-.1016633016e-9*x2^4-.1016501360e-9*x1^6-.1016397756e-9*x2^6-.9702105530e-10*x1^8-.1122593284e-9*x2^8-.1007953116e-9*x1^10-.7576559577e-10*x2^10-.9814346973e-10*x1^12-.9851479328e-10*x2^12-.1015225242e-9*x1^14-.1016671931e-9*x2^14,rational,exact):
  obj1 := 1/27+x1^2*x2^2*(x1^2+x2^2-1):
  if biteta = 64 then gmp := false: else gmp := true: fi:
  res := rootsdp(obj1,10,500,biteta,biteps,100,bitin,gmp,[],relax):
  # reshigh := rootsdp(obj1+t8,10,500,800,100,100,bitin,true,[],relax):
  return res[1]: #,reshigh[1]:
end:


# univbench(64,7,1000,5);  # 1 and 1 (eps = 1/10^3 and rank tol 1e-2 and scale = 1/10^4)
# univbench(1000,100,1000,5);  # 1 and 1 (eps = 1/10 rank tol 1e-2 and scale = 1/10^4)
# univbench(64,7,1000,5);  # 1 and 1 (eps = 1/10^3 and rank tol 1e-2 and scale = 1/10^4)
# univbench(1000,100,1000,5);  # 1 and 1 (eps = 1/10 rank tol 1e-2 and scale = 1/10^4)

# univbench(200,10); # change input precision 
# univbench(200,15);
# univbench(200,20);

# with eps = 0
# univbench(64,7,20,4);
# univbench(80,7,20,4);
# univbench(90,7,20,4);
# univbench(100,7,20,4);
# univbench(100,10,20,4);
# univbench(100,20,20,4);
# univbench(100,25,20,4);

# with eps = 1e-2
#univbench(192,50,20,10);
# .1271774025e-22*x1^14-.1150168989e-24*x1^15+.5980709238e-27*x1^16-.1512965692e-29*x1^17+.1128215351e-32*x1^18-.2787543909e-36*x1^19+.4532696404e-41*x1^20+.8538669704e-15*x1^9+.5028032750e-17*x1^10-.9604938601e-18*x1^11+.3747629567e-19*x1^12-.8681388531e-21*x1^13+.1129977777e-9-.5545670765e-9*x1+.4378818323e-9*x1^2+.5921812764e-9*x1^3-.7774643583e-9*x1^4+.2103921539e-9*x1^5-.2269817381e-10*x1^6+.1321986493e-11*x1^7-.4640022720e-13*x1^8

# univbench(193,50,20,10);
# .1191959879e-8-.1074813791e-23*x1^14+.7498542715e-26*x1^15+.2422590917e-28*x1^16-.4535667148e-30*x1^17+.1436863336e-32*x1^18-.2593185923e-36*x1^19+.2592267241e-41*x1^20-.1984143249e-14*x1^9+.6645952662e-16*x1^10-.1287764600e-17*x1^11+.1267691329e-19*x1^12-.8085966329e-23*x1^13-.3104602910e-8*x1+.3099448185e-8*x1^2-.1481429283e-8*x1^3+.2816126565e-9*x1^4+.1660790203e-10*x1^5-.3528577958e-11*x1^6-.9549579869e-13*x1^7+.3121067919e-13*x1^8

# univbench(100,12,1000,5); 1
# univbench(100,13,1000,5); 1 100
# univbench(100,18,1000,5); 1 100
# univbench(100,19,1000,5); 42
# univbench(100,20,1000,5); 51
# univbench(100,21,1000,5); 85
# univbench(100,22,1000,5); 1 100
# univbench(100,37,1000,5); 1 100
# univbench(100,38,1000,5); 67
# univbench(100,39,1000,5); 95
# univbench(100,100,1000,5);100
# univbench(300,100,1000,5);100

