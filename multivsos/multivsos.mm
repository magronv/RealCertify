#Authors: V. Magron (CNRS Verimag, Sorbonne Universite, INRIA) and M. Safey El Din (Sorbonne Universite, CNRS, INRIA)

# Algorithms implemented below are based on
# On Exact Polya and Putinar's Representations, V. Magron, M. Safey El Din,
# Proceedings of ISSAC 2018, ACM, 2018. 

$define increase_precision_rec true
$define positivity_check false
$define max_iter_prec 2

with(combinat):with(ListTools): with(ArrayTools): with(LinearAlgebra):with(convex):
# read "allbenchs.mm":

# For a given monomial, the number of mons with degree difference being at most 1 is:
# sum_{k=1}^floor(n/2) (2*k)! choose(n,2k) + 2 * sum_{k=1}^ceil(n/2) (2*k-1)! choose(n,2k-1) 
# something close to n/2 * factorial(n)

monshom := proc(n,d)
  local l1;
  l1 := [seq(1,i=1..n)];
  return map(_c -> _c - l1, combinat:-composition(n+d, n)):
end;

revmons := proc(n,d) 
  if d = 0 then 
  return [[seq(0,i=1..n)]]:
  else return [op(monshom(n,d)), op(revmons(n,d-1))]:
  fi:
end;

revmons2 := proc(n,d) 
  local mons0;
  mons0 := [[seq(0,i=1..n)]]:
  return [seq(op(monshom(n,d-i+1)),i=1..d),mons0]
end;

#Calcule les exposants de l'ensemble des monomes de degre <=d en n variables
monspt := proc(n,d)
  return ListTools:-Reverse(revmons(n,d)):
end;

mymons:=proc(vars,d)
local ms;
if d=1 then 
   return [1, op(vars)]:
else
    ms:=procname(vars,d-1):
    return [op({op(ms),
                seq(seq(v*m, v in vars), m in ms)})];
fi:
end:

mons := proc(n,d,X,ms::list := monspt(n,d))
  return map(a->mul(X[i]^a[i],i=1..n),ms);
end;


support := proc (f,X,monslist)
  local cf, idxf,monsf;
  cf := coeffs(f,X,'monsf');
  monsf := [monsf];
  idxf := seq(ListTools:-Search(m,monslist),m in monsf);
  if member(0, [idxf]) then lprint(m); error "Bug in list of monomials"; fi;
  return [cf], [idxf]; 
end;

dense_perturbation := proc(ms)
  return add(m^2,m in ms); 
end;

sos2sdp := proc(r,X,relaxorder,
                mspt,msptk,ms,msk,
                rc,ridx,e,
                precSVD::integer:=40,
                precSDP::integer:=200,
                epsStar::integer:=30,
                precOut::integer:=30,
                precIn::integer:=100,
                gmp::boolean:=false,
                algo::integer:=1,
                g::list:=[],
                gc::list:=[],
                gidx::list:=[]
                )
local start_idx, sub_idx,nvars,zero,d,k,n,ng,r0,msdp,nsdp,nblock,fd,rList,i,rfloat,j,lowerbnd,eigs,eivs,eigs2,eivs2,Y, Yrat, absorb_tbl,m, absorb_PP,nsdpg,nsdponeg,gic,giidx,ni,ig,igc,gifloat,mgi,eigsi,eivsi,nl,yMatText,objValPrimalText,mski,epsList,alg,msptgi,di,msptki;

   k := relaxorder;
   #MS: Ca c'est vraiment bizarre
   d := 2*k:
   n := nops(X);
   ng := nops(g):
   zero := [seq(0,i=1..n)]:
   msdp := nops(mspt); nsdp := nops(msk); nblock := 1+ng: 
   if FileTools:-Exists("multivsos/in.dat-s") then FileTools:-Remove("multivsos/in.dat-s"): fi:
   fd := fopen("multivsos/in.dat-s",WRITE,TEXT); 
   rList := Array([seq(0,i=1..msdp)]):
   for i from 1 to nops(rc) do  rList[ridx[i]]:=rc[i]: od:
      if mspt[1] = zero then # test whether 0 belongs the multi-index list
         sub_idx := 1:
         nvars := msdp - 1:
         r0 := rList[1]: 
      (** else
          sub_idx := ListTools:-Search(2*mspt[1],mspt)-1:
          nvars := msdp - sub_idx:
          r0 := 0:
      fi:  **)
      else 
          sub_idx := ListTools:-Search(2*mspt[1],mspt)-1:
          for j from 2 to nsdp do
              k := ListTools:-Search(mspt[1]+mspt[j],mspt):
              sub_idx := min(sub_idx,k-1):
          od:
          nvars := msdp - sub_idx:
          r0 := 0:
      fi:

      writeline(fd,convert(nvars,string)); # Number of moment variables - 1 when y0 = 1
      writeline(fd,convert(nblock,string)); # Number of blocks: 1 for the moment matrix
      fprintf(fd,"%d ",nsdp): # Size of the first block: the moment matrix has size nsdp
      nsdpg := locmatsizes(g,k,n):
      
      for ni in nsdpg do fprintf(fd,"%d ",ni): od:
      # Size of the localizing matrices: for a polynomial gi of degree di, the size is binomial(n+k-floor(di/2),n)
      fprintf(fd,"\n");

      rfloat := convert(rList,float,precIn):

    if mspt[1] = zero then 
      for i from 2 to msdp do fprintf(fd,"%.*f ",precIn, rfloat[i]);od;
    else 
      for i from 1 to nvars do fprintf(fd,"%.*f ",precIn, rfloat[i+sub_idx]);od;
    fi:
    fprintf(fd,"\n");


### The case when y0 = 1 because 0 belongs to the multi-index list
# starts with the first row of the moment matrix
  if mspt[1] = zero then 
    writeline(fd,"0 1 1 1 -1"): # Moment variable y0 = 1
    for j from 2 to nsdp do
      fprintf(fd, "%d 1 %d %d 1\n", j-1, 1, j); # Moment variable y_{j-1} at first row and column j if y0 = 1
    od;
# then the other rows of the moment matrix
    for i from 2 to nsdp do
      for j from i to nsdp do
        k := ListTools:-Search(mspt[i]+mspt[j],mspt):
        fprintf(fd, "%d 1 %d %d 1\n", k-1, i, j); # Moment variable y_{k-1} at row i and column j if y0 = 1
      od:
    od:
  else
     for i from 1 to nsdp do
      for j from i to nsdp do
        k := ListTools:-Search(mspt[i]+mspt[j],mspt):
        #if i = 1 then lprint(ms[i]); lprint(ms[j]); lprint(ms[i]*ms[j]); lprint(k): fi:
        fprintf(fd, "%d 1 %d %d 1\n", k-sub_idx, i, j); # Moment variable y_k at row i and column j
      od:
    od:
  fi:
# then the localizing matrices
for ig from 1 to ng do
   gic := gc[ig]: giidx := gidx[ig]: 
   for igc from 1 to nops(gic) do
     msptgi := monspt(n,d/2 - ceil(degree(g[ig])/2)):
     mgi := mspt[giidx[igc]]:
     #lprint(mgi);
     gifloat := convert(gic[igc],float,precIn):
     if mgi = zero then 
      fprintf(fd, "0 %d 1 1 %.*f\n", ig+1,precIn,-gifloat); # The entry is gifloat y_0
      for j from 2 to nsdpg[ig] do
        fprintf(fd, "%d %d 1 %d %.*f\n", j-1, ig+1, j, precIn, gifloat); # Moment variable y_{j-1} at first row and column j since y0 appears
      od;
     for i from 2 to nsdpg[ig] do
       for j from i to nsdpg[ig] do
         k := ListTools:-Search(msptgi[i]+msptgi[j]+mgi,mspt):
         fprintf(fd, "%d %d %d %d %.*f\n",k-1, ig+1, i, j, precIn, gifloat); # Moment variable y_{k-1} at row i and column j since y0 appears
       od:
     od: 
     else
     for i from 1 to nsdpg[ig] do
       for j from i to nsdpg[ig] do
         # lprint(msptgi[i]+msptgi[j]+mgi);
         k := ListTools:-Search(msptgi[i]+msptgi[j]+mgi,mspt):
         # lprint(k);
         fprintf(fd, "%d %d %d %d %.*f\n",k-sub_idx, ig+1, i, j, precIn, gifloat); # Moment variable y_k at row i and column j
       od:
     od: 
     fi:
   od:
od:

### The case when y0 = 1 because 0 does not belong to the multi-index list
# there is a bug in sdpa-gmp when not subtracting the "useless" moment variables with k - sub_idx
(**
  else
  for i from 1 to nsdp do
    for j from i to nsdp do
      k := ListTools:-Search(msptgi[i]+msptgi[j],mspt):
      # lprint(k);
      fprintf(fd, "%d 1 %d %d 1\n", k-sub_idx, i, j); # Moment variable y_{k-sub_idx} at row i and column j
    od;
  od;
  fi:
**)
#### Loop again to find the absorbers (this loop can be saved thanks to the previous one)
  absorb_tbl := table([seq(0,i=1..msdp)]):
  for i from 1 to nsdp do
    for j from i to nsdp do
      m := mspt[i]+mspt[j]:
            if not_even(m) then k:= ListTools:-Search(m,mspt): absorb_tbl[k]:=i: fi: 
    od:
  od:

#### In this table, for each gamma (appearing at index k in mspt), one indicates all monomials alpha (and implicitely beta) such that gamma = alpha + beta 
  absorb_PP := table([seq([],i=1..msdp)]):
  for i from 1 to nsdp do
  k:= ListTools:-Search(2*mspt[i],mspt): absorb_PP[k]:=[op(absorb_PP[k]),i,i]:
    for j from i+1 to nsdp do
      m := mspt[i]+mspt[j]: k:= ListTools:-Search(m,mspt): absorb_PP[k]:=[op(absorb_PP[k]),i,j,j,i]:
    od:
  od:
  #lprint(msdp); lprint(mspt); lprint(absorb_PP);

#### old version, works only when all monomials belong to the NP
#  for j from 2 to nsdp do
#    fprintf(fd, "%d 1 %d %d 1\n", j-1, 1, j); # Moment variable y_{j-1} at first row and column j
#  od;
#  for i from 2 to nsdp do
#    for j from i to nsdp do
#      k := Search(mspt[i]+mspt[j],mspt):
#      fprintf(fd, "%d 1 %d %d 1\n", k-1, i, j); # Moment variable y_{k-1} at row i and column j
#    od;
#  od;

  fclose(fd);

  # SED commands perform as in univsos/univsos3.mm
  # ssystem("sed -i 's/ \\./ 0\\./g' multivsos/in.dat-s"); ssystem("sed -i 's/^\\./0\\./g' multivsos/in.dat-s"); ssystem("sed -i 's/^-\\./-0\\./g' multivsos/in.dat-s"); ssystem("sed -i 's/ -\\./ -0\\./g' multivsos/in.dat-s");

  if FileTools:-Exists("multivsos/out.dat-s") then FileTools:-Remove("multivsos/out.dat-s"): fi:
  if FileTools:-Exists("multivsos/out.mm") then FileTools:-Remove("multivsos/out.mm"): fi:

  if not gmp then
    ### OLD ssystem("sdpa -ds in.dat-s -o out.dat-s -p param.sdpa > /dev/null");
    ssystem("sdpa -ds multivsos/in.dat-s -o multivsos/out.dat-s -p multivsos/param.sdpa > /dev/null");
  else
    write_param(precSDP,epsStar,epsStar);
    ### OLD ssystem("sdpa_gmp -ds in.dat-s -o out.dat-s -p my_param_gmp.sdpa > /dev/null");
    ssystem("sdpa_gmp -ds multivsos/in.dat-s -o multivsos/out.dat-s -p multivsos/my_param_gmp.sdpa > /dev/null");
  fi:

  # the GREP/SED commands looks for the values of objValPrimal and yMat

  ### OLD ssystem("echo $(grep objValPrimal multivsos/out.dat-s) ';' 'yMat:=' $(sed -n '/yMat/,/main/{//!p}' multivsos/out.dat-s) ';' >> multivsos/out.mm");
  ### OLD ssystem("sed -i 's/ =/ :=/g' multivsos/out.mm"): 
  ### OLD ssystem("sed -i 's/{/[/g' multivsos/out.mm"): ssystem("sed -i 's/}/]/g' multivsos/out.mm"); ssystem("sed -i 's/] \\[/], \\[/g' multivsos/out.mm");

  fd := fopen("multivsos/out.dat-s",READ,TEXT):
  nl := "":  yMatText := "yMat := ":  
  while true do     
    nl := readline(fd): 
    if SearchText("objValPrimal",nl) = 1 then objValPrimalText := StringTools[RegSubs]("objValPrimal = (.*)" = "\\1",nl): break: fi:
  od:
  while true do     
    nl := readline(fd): 
    if SearchText("yMat",nl) = 1 then break: fi:
  od:
  while true do
    nl := readline(fd): 
    if SearchText("main loop",nl) > 0 then break: fi:
    yMatText := cat(yMatText,nl):
  od:
  fclose(fd):
  yMatText := StringTools[SubstituteAll](yMatText, "{", "["): yMatText := StringTools[SubstituteAll](yMatText, "}", "]"): yMatText := StringTools[SubstituteAll](yMatText, "][", "],["):
  fd := fopen("multivsos/out.mm",WRITE,TEXT);
#  lprint(objValPrimalText);lprint(yMatText);
  fprintf(fd,"objValPrimal := %s:\n%s:",objValPrimalText,yMatText);
  fd := fclose(fd):
  read "multivsos/out.mm":
  printf("Numerical SDP ended\n");
  # OLD ABSORBTION if algo = 1 then lowerbnd := r0 + convert(objValPrimal,rational,exact) : else lowerbnd := 0: fi:
  lowerbnd := r0 + convert(objValPrimal,rational,exact) :

  printf("Lower bound: %f\n", evalf(lowerbnd));
  eigs:=Array([]); eivs := Array([]);
  nsdponeg:=[nsdp,op(nsdpg)]:
  alg := 1:
  for j from 1 to ng+1 do
    i := ng + 2 -j:
    Y := Matrix(yMat[i]): 
    if i > 1 then di := d/2 - ceil(degree(g[i-1])/2): msptki := monspt(n,di): mski := mons(n,di,X,msptki): (** mski := msk[1..nsdponeg[i]]:**) else mski := msk: fi:
    if algo = 1 or i > 1 then Yrat:=Matrix(Y): 
    else # case when algo = 2 and i = 1
     if precOut >= 30 then Y := convert(Y,rational,exact): else Y := convert(Y,rational,precOut) fi:
     Y := (Y + Transpose(Y))/2;
     epsList := epsPP(convert(eigs,list),convert(eivs,list),r,g,nsdpg,X,ms,msdp);

     Yrat := absorber_PP(Y,epsList,absorb_PP,nsdp,msptk, mspt):
     # printf("err="); #lprint(expand(Transpose(Vector(msk)).Yrat.Vector(msk)+lowerbnd-r)) 
    fi:
    if (algo = 2 or algo = 3) and i = 1 then alg := algo: fi:
    (eigsi,eivsi) := eigseivs(Yrat,X,mski,precSVD,precOut,alg,e);
    eigs:=Concatenate(1,eigsi,eigs):  eivs:=Concatenate(1,eivsi,eivs):
   od:
  return convert(eigs,list), convert(eivs,list),lowerbnd,absorb_tbl,nsdponeg:

     #if ng = 0 then Yrat := absorber_PP(Y,rList,absorb_PP,nsdp,mspt): else Yrat:=Y: fi:
     #printf("err="); #lprint(expand(Transpose(Vector(msk)).Yrat.Vector(msk)+lowerbnd-r)) 

    #fi:
    #(eigsi,eivsi) := eigseivs(Yrat,X,msk[1..nsdponeg[i]],precSVD,precOut,algo);
    #eigs:=Concatenate(1,eigs,eigsi):  eivs:=Concatenate(1,eivs,eivsi):
   #od:
  #return convert(eigs,list), convert(eivs,list),lowerbnd,absorb_tbl,nsdponeg:
end;

epsPP := proc(eigs,eivs,r,g,nsdpg,X,ms,msdp)
  local idx,i,sumsos,idxi,epsList,epsc,epsidx;
  idx := 0: 
  sumsos := r:
  for i from 1 to nops(g) do
    idxi := idx + nsdpg[i]:
    sumsos := sumsos - g[i]*sum(eigs[j]*eivs[j]^2,j=idx+1..idxi);
    idx := idxi:
  od:
  epsc,epsidx := support(expand(sumsos),X,ms):

  epsList := Array([seq(0,i=1..msdp)]):
  for i from 1 to nops(epsc) do epsList[epsidx[i]]:=epsc[i]: od:
  return epsList:
end:


absorber_PP := proc(Yrat,rList,absorb_PP,nsdp,msptk,mspt)
  local Yproj,i,j,i1,k,m,list_m,nopsm,maxY,minY,yij;
  # for i from 1 to nsdp do
  #  for j from i to nsdp do 
  #    yij := Yrat[i,j]: if abs(yij) < 1/10^3 then Yrat[i,j] := 0: Yrat[j,i]:=0: fi:
  #  od:
  #od:
   #Yproj := Matrix(Yrat,shape='symmetric');
   Yproj := Matrix(nsdp,nsdp,shape='symmetric');
   maxY := max(max(abs(Yproj))):    minY := min(min(abs(Yproj))): 
   #lprint(evalf(minY)); lprint(evalf(maxY)); #lprint(Y); #lprint(rList);
   for i from 1 to nsdp do
    for j from i to nsdp do 
      m := msptk[i] + msptk[j]; k:= ListTools:-Search(m,mspt):
      list_m := [op(absorb_PP[k])]: nopsm := nops(list_m)/2:
      Yproj[i,j] := Yrat[i,j] - 1/nopsm*(add(Yrat[list_m[2*im-1],list_m[2*im]],im=1..nopsm) - rList[k]):
    od:
  od:
  return Yproj: 
end:

write_param := proc(precSDP,epsStar,epsDash)
  local fd;
  fd := fopen("multivsos/my_param_gmp.sdpa",WRITE,TEXT); 
  fprintf(fd,"300	unsigned int maxIteration;\n");
  fprintf(fd,"1.0E-%d	\t double 0.0 < epsilonStar;\n",epsStar);
  fprintf(fd,"1.0E5   double 0.0 < lambdaStar;\n");
  fprintf(fd,"2.0   	double 1.0 < omegaStar;\n");
  fprintf(fd,"-1.0E5  double lowerBound;\n");
  fprintf(fd,"1.0E5   double upperBound;\n");
  fprintf(fd,"0.1     double 0.0 <= betaStar <  1.0;\n");
  fprintf(fd,"0.3     double 0.0 <= betaBar  <  1.0, betaStar <= betaBar;\n");
  fprintf(fd,"0.9     double 0.0 < gammaStar  <  1.0;\n");
  fprintf(fd,"1.0E-%d	\t double 0.0 < epsilonDash;\n",epsDash);
  fprintf(fd,"%d     precision\n",precSDP);
  fprintf(fd,"%%+50.40Fe     char* \t xPrint \t   (default %%+8.3e, \t    NOPRINT skips printout) \n");
  fprintf(fd,"%%+50.40Fe     char* \t XPrint \t   (default %%+8.3e, \t    NOPRINT skips printout) \n");
  fprintf(fd,"%%+50.40Fe     char* \t YPrint \t   (default %%+8.3e, \t    NOPRINT skips printout) \n");
  fprintf(fd,"%%+50.40Fe     char* \t infPrint \t (default %%+10.16e, \t  NOPRINT skips printout) \n");
  fclose(fd);
end;

checkrational := proc(U)
  local v:
  for v in U do:
    if not type(convert(v,rational),realcons) then 
#    lprint(v): 
    error "Complex Cholesky factor, retry with gmp = true or more SDP precision":fi:
  od:
  return:
end;

eigseivs := proc(Yrat,X,ms,precSVD,precOut,algo,e)
  local v, msvec, eigs, eivs, U,S,V, Ysvd,SVD,ti,tcmp;
  SVD := false:
  msvec := Vector(ms);
  # if SVD then 
  #  Digits:=precSVD;
  #  lprint("starting SVD");
  #  ti := time():
  #  (U,S,V) := MTM[svd](Yrat); 
  #  tcmp := time() - ti: lprint (tcmp);
  #  lprint("ending SVD");
  #else
    ti := time():
    printf("Cholesky decomposition ... ");
    S := IdentityMatrix(nops(ms));
    if algo = 1 or algo = 3 then Digits := precSVD:fi:
    if algo < 3 then 
      U := LUDecomposition(Yrat,method='Cholesky'); 
    else
      U := LUDecomposition(Yrat - e*S,method='Cholesky');
    fi:
    checkrational(U):
    tcmp := time() - ti:
    #lprint(U);
    #lprint (tcmp);
    printf("done.\n");
  # fi:
    eigs := Diagonal(S); eivs := Transpose(Transpose(msvec).U);
    Digits:=10;
    Ysvd := U.S.V^%T;
    #lprint(Ysvd); #lprint(max(max(Y),-min(Y))); #lprint(max(max(Ysvd),-min(Ysvd))); #lprint(max(Y - Ysvd));
    if precOut >= 30 then #lprint("exact");
       eigs := convert(eigs,rational,exact);
       eivs := map (_e -> convert(_e,rational,exact), eivs):
    else
        eigs := convert(eigs,rational,precOut);
        eivs := map (_e -> convert(_e,rational,precOut), eivs);
    fi:
    return (eigs, eivs):
end;


# Unused function
# neighbours1 := proc(n)
#   local i,nones,nmones,nzeros,nlist,n1;
#   nlist := [];
#   for i from 1 to floor(n/2) do
#     nones:=i; nmones:=i; nzeros:=n-2*i; nlist:=[op(nlist),op(permute([1$nones, 0$nzeros, -1$nmones]))];
#   od:
#   for i from 1 to ceil(n/2) do
#     nones:=i; nmones:=i-1; nzeros:=n-2*i+1; n1 := permute([1$nones, 0$nzeros, -1$nmones]);
#     nlist := [op(nlist),op(n1),op(-n1)]:
#   od:
#   return nlist:
# end;

# Unused function
# count_n1 := proc(n)
#   add(multinomial(n,i,i,n-2*i),i=1..floor(n/2)) + 2*add(multinomial(n,i,i-1,n-2*i+1),i=1..ceil(n/2));
# end;

# Unused function
# even_n1 := proc(n,mspt)
#   local n1,a,list_n1,cardn1,n1a,b,ab;
#   list_n1 := [];
#   n1 := neighbours1(n); 
#   cardn1 := nops(n1);
#   for a in mspt do:
#     # n1a := [];
#     # for b from 1 to cardn1 do 
#     #   ab := a+n1[b]:
#     #   if not has(ab,-1) then n1a := [op(n1a),ab]:fi:
#     # od:
#     n1a:=map(ab->if  has(ab, -1) then ab fi, [seq(a+b, b in n1)]):
#     list_n1:=[op(list_n1),n1a]:
#   od:
#   return list_n1:
# end;

not_even := proc(m)
  return has(1,map(_c -> irem(_c,2), m)):
end;

### This procedure sometimes fails to find absorbing polynomials since the resulting decomposition yields monomials whose squares do not necessarily belong to the NP
decomp_mon := proc(a,n)
  local c,b,degb,cm,cnt,i;
  c := map(_c -> iquo(_c,2), a);
  b := a - 2*c;
  degb := ceil(add(i,i in b)/2);
  cm := [seq(0,i=1..n)]:
  cnt := 0;
  for i from 1 to n do:
    if b[i] = 1 then cm[i] := b[i]: cnt := cnt + 1: fi:
    if cnt >= degb then break: fi:
  od:
  return c,cm,b - cm;
end;

NP_PolyhedralSets := proc(f,X,mspt,ms)
  local cf, idxf,msptf,ps;
  cf, idxf := support(f,X,ms):
  msptf := map (_idx -> mspt[_idx], idxf): 
#  lprint(msptf);
  ps := PolyhedralSets:-PolyhedralSet(msptf):
  return PolyhedralSets:-ConvexHull(ps):
end;

mytruncate_withNP := proc(f,X,mspt,msptk,ms,use_convex)
  local NP,msptkNP,msptNP:

  NP := newtonpolytope(f, X): 
  msptkNP:=map(m->if contains(NP,2*m) then m fi, msptk):

  #msptNP:=map(m->if contains(NP,m) then m fi, mspt):

  msptNP:=[op(msptkNP),op(map(m->if contains(NP,m) then m fi,

  #Bug on some polynomials when removing monomials in msptk but ok
  #when removing monomials in msptkNP which is what we meant.

  remove(member, mspt, msptkNP)))]:
  return msptNP,msptkNP:
end:


#MS: a voir avec Victor
truncate_withNP := proc(f,X,mspt,msptk,ms,use_convex)
  local NP,msptkNP,msptNP,m,i,newmspt;

  return mytruncate_withNP(f,X,mspt,msptk,ms,use_convex);
(**
  if use_convex then
    NP := newtonpolytope(f, X): 
  else NP:=NP_PolyhedralSets(f,X,mspt,ms): fi:
  #msptkNP := [];
       #for m in msptk do if contains(NP,2*m) then msptkNP := [op(msptkNP),m] : fi: od:
  if use_convex then
    msptkNP:=map(m->if contains(NP,2*m) then m fi, msptk):
  else msptkNP:=map(m->if 2*m in NP then m fi, msptk): fi:
  msptNP := [op(msptkNP)]:
  if use_convex then
     newmspt:=remove(member, mspt, msptk);
     msptNP:=[op(msptNP), op(map(m->if contains(NP,m) then m fi, newmspt))];
(**    for i from nops(msptk) + 1 to nops(mspt) do
      m := mspt[i]:
      if contains(NP,m) then msptNP := [op(msptNP),m] : fi:
         od:
         **)
  else
    for i from nops(msptk) + 1 to nops(mspt) do
      m := mspt[i]:
      if m in NP then msptNP := [op(msptNP),m] : fi:
    od:
  fi:
  return msptNP,msptkNP:
**)
end:

testNP := proc(f,X,use_convex)
  local d,k,n,card_nk,mspt,msptk,ms;
  d := degree(f): k := d/2: n := nops(X): card_nk := binomial(n+k,k):
  mspt := monspt(n,d): msptk := mspt[1..card_nk]: ms := mons(n,d,X,mspt):
  return truncate_withNP(f,X,mspt,msptk,ms,use_convex):
end:

absorber := proc(u,X,e,even_mons,ms,mspt,absorb_tbl)
  local i,j,k,ucoeffs,uidx,uc,err_list,err,m,bad_m,n,c,cm,cp,cfs,sqs,n1,m1,m2,k1,k2;
  #printf("\nu = "); #lprint(evalf(u));
  ucoeffs,uidx := support(u,X,ms);
  n := nops(X): 
  #printf("\neven_mons = "); #lprint(even_mons);
  err_list := Array([seq(e,i=1..nops(even_mons))]); 
  cfs := []; sqs := [];
  for i from 1 to nops(ucoeffs) do
    uc := ucoeffs[i]; bad_m := mspt[uidx[i]]:
    #lprint(uc); lprint(bad_m);

    if not_even(bad_m) then
      k := ListTools:-Search(bad_m,mspt):
      m1:=mspt[absorb_tbl[k]]:m2:=bad_m - m1;
      #lprint(bad_m); lprint(m1); lprint(m2);
      cfs := [op(cfs),abs(uc)/2]; 
      sqs := [op(sqs),mul(X[i]^m1[i],i=1..n)+sign(uc)*mul(X[i]^m2[i],i=1..n)]:
      k1:=ListTools:-Search(2*m1,even_mons): k2:=ListTools:-Search(2*m2,even_mons):
      err_list[k1] := err_list[k1]-1/2*abs(uc): err_list[k2] := err_list[k2]-1/2*abs(uc):
    else 
      k := ListTools:-Search(bad_m,even_mons): err_list[k]:=err_list[k]+uc:
    fi:
  od:
  err_list := convert(err_list,list);
  # printf("\nerr list = "); #lprint(evalf(err_list));
  cfs := [op(cfs),op(err_list)]; sqs := [op(sqs),seq(mul(X[i]^(m[i]/2),i=1..n), m in even_mons)];
  return cfs,sqs:
end;


old_absorber := proc(u,X,e,even_mons,ms,mspt,even_mons_n1,absorb_tbl)
  local i,j,k,ucoeffs,uidx,uc,err_list,err,m,bad_m,n,c,cm,cp,cfs,sqs,n1,m1,m2;
  printf("\nu = "); lprint(evalf(u));
  ucoeffs,uidx := support(u,X,ms);
  n := nops(X): #bm := [seq(0,i=1..n)]: bp := bm: 
  #for i from 1 to ceil(n/2) do bm[i]:=1:od:for i from ceil(n/2)+1 to n do bp[i]:=1:od:
  #card_absorbed := []; # counts how many monomials to absorbe for each even monomial with support in even_mons
  # printf("\neven_mons = "); lprint(even_mons);
  err_list := [];
  for j from 1 to nops(even_mons) do
    m := even_mons[j];
    #lprint(m);
    n1 := even_mons_n1[j];
    #lprint(n1);
    err := e;
    for i from 1 to nops(ucoeffs) do
      # printf("0 "); lprint(bad_m); lprint(err);
      # printf("1 "): lprint(bad_m); lprint(err);
      uc := ucoeffs[i]; bad_m := mspt[uidx[i]]:
      if m = bad_m then err := err + uc: else if has(bad_m,n1) then err := err - 1/2*abs(uc): fi:fi:
    od:
    err_list := [op(err_list),err];
  od:
  printf("\nerr list = "); #lprint(evalf(err_list));
  cfs := [op(err_list)]; sqs := [seq(mul(X[i]^(m[i]/2),i=1..n), m in even_mons)];
  for i from 1 to nops(ucoeffs) do
    uc := ucoeffs[i]; bad_m := mspt[uidx[i]]:
    if not_even(bad_m) then
    #c,cm,cp := decomp_mon(bad_m,n); 
    #printf("\n"); lprint(bad_m);
    #lprint(c+cm); lprint(c+cp);
    #m1 := c+cm: m2 := c + cp:
    k := ListTools:-Search(bad_m,mspt):m1:=mspt[absorb_tbl[k]]:m2:=bad_m - m1;
    printf("\n"); lprint(bad_m);
    lprint(m1); lprint(m2);
    cfs := [op(cfs),abs(uc)/2]; 
    sqs := [op(sqs),mul(X[i]^m1[i],i=1..n)+sign(uc)*mul(X[i]^m2[i],i=1..n)]: fi:
  od:
  return cfs,sqs:
end;

relaxordermin := proc(g)
 return ceil(max(seq(degree(gi),gi in g))/2):
end:

locmatsizes := proc(g,k,n)
  return [seq(binomial(n + k - ceil(degree(gi)/2),n),gi in g)]:
end;

perturbate := proc(p,epsilon)
  local X,d,k,n,mspt,card_nk,msptk,ms,msk;
  X:=[op(indets(p))]:
  d := degree(p): k := ceil(d/2);
  n := nops(X): 
  mspt := monspt(n,d):
  card_nk := binomial(n+k,n);
  msptk := mspt[1..card_nk];
  mspt,msptk := truncate_withNP(p,X,mspt,msptk):
  ms := mons(n,d,X,mspt): msk := mons(n,k,X,msptk):
  return p + 2^(-epsilon)*dense_perturbation(msk):
end:


preliminary_test_negativity:=proc(p, X, d, k)

  #MS: a ameliorer ; ne pas considerer lcoeff uniquement
  if lcoeff(p) < 0 and ng = 0 then lprint(p): error "There is no decomposition into sum of squares for this polynomial"; fi;

  if subs({seq(x=0, x in X)}, p) <0 and ng =0 then
     lprint(p);
     error "There is no decomposition into sum of squares for this polynomial";
  fi;

  # Let d = 2 k be the degree; fail if this is odd (can't be pos def) 
  # If it's a constant (i.e. the polynomial is a constant multiple of 
  # a perfect square) then return almost immediately.                 

  if (2 * k <> d and ng = 0) then
     lprint(p): error "There is no decomposition into sum of squares for this polynomial";
  fi;
  if (d = 0 and ng = 0) then lprint(p, " * (", s, ")^2"); fi;

end:

##############################################################################
##############################################################################
# Basic functions for implementing the degree principle (see Riener et al.)
##############################################################################
##############################################################################

permutepoly := proc(f, sigma, vars)
      local se, n, lsa, fsub,ls;

      n := numelems(vars);

      se := [seq(cat('u', sigma[i]), i = 1..nops(sigma))];
      ls := [seq(vars[i] = se[i], i = 1..nops(se))];

      fsub := subs(ls,f);
      
      return fsub;
end:

IsSymmetric := proc(f, vars)
local n, i, S;

               n := nops(vars);

               S := [[1, 2], [seq(i, i=1..n)]];

               for i from 1 to nops(S) do:
                   if expand(permutepoly(f, S[i], vars) - f) = 0  then
                      return true;
                   else
                        return false;
                   end if;
               end do;
end:

##############################################################################
##############################################################################

is_homogeneous:=proc(f)
local vars, l, lf;
vars:=indets(f):
lf:=subs({seq(v=l*v, v in vars)}):
if expand(lf-l^degree(f)*f)=0 then return true; else return false; fi;
end:

##############################################################################
##############################################################################

multivsos:=proc(f, glist::list:=[], relaxorder::integer :=0, algo::integer := 1, denom::boolean:=false)
local sos, nsdponeg, X, ti, tf, vars, oldvars, b;
  ti := time[real](); 
  oldvars:=[op(indets([f,op(glist)]))]:
  vars := [seq(cat('u',i), i=1..nops(oldvars))]:
  #This call is naive and should be improved (parameters should be set according to f
   b :=multivsos_internal(subs({seq(oldvars[i]=vars[i], i=1..nops(vars))},f),
  ':-glist'=subs({seq(oldvars[i]=vars[i], i=1..nops(vars))},glist),':-relaxorder'=relaxorder, ':-algo'=algo,':-denom'=denom);
    tf := time[real]()-ti; 
    printf("Total time= %fsecs\n",tf);
    if nops(b) = 4 then  
    # {bitsos,tf,nsdponeg,sos}
      sos := b[4]:
      return {b[1],b[2],b[3],subs({seq(vars[i]=oldvars[i], i=1..nops(vars))},sos)}:
    else return b:
    fi:
#  return {b[1],b[2],b[3],subs({seq(vars[i]=oldvars[i], i=1..nops(vars))},sos),subs({seq(vars[i]=oldvars[i], i=1..nops(vars))},)}:
end:

multivsos_internal:=proc(f,
                 { 
                 epsilon::integer := 8,
                 precSVD::integer := 40,
                 precSDP::integer := 100,
                 epsStar::integer := 5,
                 precOut::integer:=30,
                 precIn::integer:=100,
                 gmp::boolean:=false,
                 algo::integer:=1,
                 glist::list:=[],
                 relaxorder::integer:=0,
                 denom::boolean:=false,
                 iter_prec::integer:=0
                 })
local p,d,mspt,ms,rc,ridx,pc,pidx,ng,gc,gidx,gic,giidx,S,s,c, q,n,k,t,e,r,l,a,s1,s2,u,v,i,j,sqs,cfs,sos,rfloat, eigs, eigsg, eigsgi, soslist,soslistg, soslistgi, sumsos,cnd,maxq,obj_plus_r0, card_nk, even_mons,err_list,err,msptk,msk,absorb_tbl,rmin,nsdponeg,idx,oneg,idxi,g,X,ti,tf,gi,cg,use_convex,newf,hf,vars,deg_denom,mspt_denom,ms_denom,msptk_denom,msk_denom,sigma1,bitsos;
  use_convex := true:
  ti := time[real]():

(**
  if nops(glist)=0 and positivity_check then
     if is_homogeneous(f) then
       newf:=subs({indets(f)[1]=1}, f):
     else
       newf:=f:
     fi:  
     hf:=subs(h=0,numer(subs({seq(v=v/h,v in indets(f))},newf))):
     vars:=[op(indets(hf))]:
     hf:=subs(vars[1]=1, hf):
     if nops(RAG:-HasRealSolutions([hf=0]))>0 or subs({seq(v=0,v in vars)}, hf)<=0 then
        error "Unconstrained case: assumptions are not satisfied (homogenized input polynomial has non-trivial zeros at infinity)"
     fi:
  fi:
**)

  c := max(map(_c -> abs(_c),coeffs(expand(f))));
  c := max(c,1):
  p := 1/c*f;
  X := [op(indets([p,op(glist)]))]:
  g := [];
  ng := nops(glist);
  for i from 1 to ng do
    gi := expand(glist[i]);
    cg := max(map(_c -> abs(_c),coeffs(expand(gi)))):
    #cg := max(cg,1);
    cg := 1;
    g := [op(g),gi/cg]
  od:
  #g := expand(glist);

  n := nops(X):
 (** if ng = 0 then
     d := degree(p):
  else**)
     rmin := relaxordermin([p,op(g)]):
     d := 2*max(relaxorder,rmin):
  #fi:
  k := ceil(d/2);

  #Liste des exposants des monomes de degre <=d en n variables
  mspt := monspt(n,d):
  card_nk := binomial(n+k,n);
  #Liste des exposants des monomes de degre <=k en n variables
  msptk := monspt(n, k): #mspt[1..card_nk];

  ms := mons(n,d,X,mspt):
  lprint(X);
  printf("Polynomial system with %d variables and degree at most %d\n",n,d);
  printf("Size of monomial basis = %d\n",nops(msptk));
  printf("Number of moment variables = %d\n",nops(ms));

  if ng = 0 then
     mspt,msptk := truncate_withNP(p,X,mspt,msptk,ms,true):
       printf("\nAfter Newton polytope reduction:\n");
       printf("Size of monomial basis = %d\n",nops(msptk));
       printf("Number of moment variables = %d\n",nops(mspt));
  fi: # if no constraints then compute NP
  if ng = 1 and denom then
     deg_denom := d - degree(glist[1]):
     mspt_denom := monspt(n,deg_denom): ms_denom := mons(n,deg_denom,X,mspt_denom):
     msptk_denom := monspt(n,deg_denom/2): msk_denom := mons(n,deg_denom/2,X,msptk_denom):
     sigma1 := expand((add(m,m in msk_denom))^2):
     mspt,msptk := truncate_withNP(glist[1]*sigma1,X,mspt,msptk,ms,true):
       printf("\nAfter Newton polytope reduction:\n");
       printf("Size of monomial basis = %d\n",nops(msptk));
       printf("Number of moment variables = %d\n",nops(mspt));
  fi: # if glist := [-f] and denom = true, the goal is to prove that f >= 0 then compute NP of the numerator = sigma0 (s.t. sigma0 - f * sigma1 = 0)


  ms := mons(n,d,X,mspt):
  msk := mons(n,k,X,msptk):

  # let r = p - e * t be a safe perturbation 
  if algo = 1 or algo = 3 then
  t := dense_perturbation(msk);
  # printf("\nperturber = "); lprint(t):
  even_mons := 2*msptk;
  e := 1/2^epsilon: 
  else 
      e := 0: t := 0: 
  fi:
  



  r := expand(p - e*t);

  if nops(glist)=0 and positivity_check then
    if is_homogeneous(r) then
       newf:=subs({indets(r)[1]=1}, r):
    else
       newf:=f:
    fi:  
    vars:=[op(indets(newf))]:
    if nops(RAG:-HasRealSolutions([newf=0]))>0 then
        error "Unconstrained case: assumptions are not satisfied (homogenized input polynomial has non-trivial zeros at infinity)"
    fi:
  fi:
  pc,pidx := support(expand(p),X,ms):
  if r = 0 then rc:=[0]: ridx := [1] else rc,ridx := support(r,X,ms):fi:
  #printms(ms);

  gc := []:
  gidx := []:

  for i from 1 to ng do 
    gic,giidx := support(g[i],X,ms):
    gc:=[op(gc),gic]; gidx:=[op(gidx),giidx];
  od:
  printf("SDPA starts...\n");
  if algo = 3 then (eigs,soslist,obj_plus_r0,absorb_tbl,nsdponeg) :=
  sos2sdp(p,X,k,mspt,msptk,ms,msk,pc,pidx,e,precSVD,precSDP,epsStar,precOut,precIn,gmp,algo,g,gc,gidx);
  else
  (eigs,soslist,obj_plus_r0,absorb_tbl,nsdponeg) :=
  sos2sdp(r,X,k,mspt,msptk,ms,msk,rc,ridx,e,precSVD,precSDP,epsStar,precOut,precIn,gmp,algo,g,gc,gidx);
  fi:
  idx := 0: sumsos := obj_plus_r0: oneg := [1,op(g)];
  for i from 1 to ng+1 do
    idxi := idx + nsdponeg[i]:
    sumsos := sumsos + oneg[i]*sum(eigs[j]*soslist[j]^2,j=idx+1..idxi);
    idx := idxi:
  od:
#  sumsos := obj_plus_r0 + sum(eigs[j]*soslist[j]^2,j=1..nops(soslist));
#  for i from 1 to ng do 
#    eigsgi := eigsg[i]: soslistgi := soslistg[i]:
#    sumsos := sumsos + sum(eigsgi[j]*soslistgi[j]^2,j=1..nops(soslistgi))*g[i];
#  od:

  if algo = 1 or algo = 3 then
    ## if ng > 0 then
    ## t := dense_perturbation(msk);
    ## printf("\nperturber = "); lprint(t):
    ## even_mons := 2*msptk;e := 1/2^epsilon: r := expand(p - e*t);
    ## fi:
    u := expand(r - sumsos+obj_plus_r0);
    #lprint(soslist); lprint(u);
    try 
      cfs,sqs := absorber(u,X,e,even_mons,ms,mspt,absorb_tbl);
    catch "invalid subscript selector":
      lprint("Invalid absorbtion table, certainly a Newton polytope issue"):  return false:
    end try:
    nsdponeg[1]:=nsdponeg[1]+nops(sqs):
  else
    u:=0;
    cfs := []: sqs := []:
  fi:
  err := expand(u+e*t - add(cfs[i]*sqs[i]^2,i=1..nops(sqs)));
#  if algo = 1 then printf("\nerr = "); lprint(evalf(err));
#  fi:
  cfs := [op(cfs), op(eigs)]: sqs := [op(sqs),op(soslist)]:
  sos := [];
  for i from 1 to nops(sqs) do
    sos := [op(sos), c*cfs[i],sqs[i]]:
  od;
  
  if soscheck1(f,sos,nsdponeg,g) = 1 then
    printf("\n Exact sum of squares decomposition\n"):
    tf := time[real]()-ti; 
    bitsos := BitSizePolSeq(sos,X);
    printf("bitsize= %d\n",bitsos);
    printf("time= %fsecs\n",tf);
    return {bitsos,tf,nsdponeg,sos}:
  else 
    if not increase_precision_rec or iter_prec >= max_iter_prec then 
      printf("\n Aborting \n"): return {false}: fi:
    printf("\n***************************\n");
    printf("****   RECURSIVE CALL  ****\n");
    lprint(2*epsilon,
      2*precSVD,
      2*precSDP,
      2*epsStar,
      2*precOut,
      2*precIn
      ); 
    printf("***************************\n\n");
    if epsilon<=16 then     
    return multivsos_internal(f,
      ':-epsilon'=2*epsilon,
      ':-precSVD'=2*precSVD,
      ':-precSDP'=2*precSDP,
      ':-epsStar'=2*epsStar,
      ':-precOut'=2*precOut,
      ':-precIn'=2*precIn, 
      ':-gmp'=gmp, 
      ':-algo'=algo, 
      ':-glist'=glist, 
      ':-relaxorder'=relaxorder,
      ':-denom'=denom,
      ':-iter_prec'=iter_prec+1
      )
    else
      return multivsos_internal(f,
      ':-epsilon'=2*epsilon,
      ':-precSVD'=2*precSVD,
      ':-precSDP'=2*precSDP,
      ':-epsStar'=2*epsStar,
      ':-precOut'=2*precOut,
      ':-precIn'=2*precIn, 
      ':-gmp'=true, 
      ':-algo'=algo, 
      ':-glist'=glist, 
      ':-relaxorder'=relaxorder,
      ':-denom'=denom,
      ':-iter_prec'=iter_prec+1
      );
      fi; 
    fi:
  tf := time[real]()-ti; 
  printf("bitsize= %d\n",BitSizePolSeq(sos,X));
  printf("time= %fsecs\n",tf);
end;

soscheck1:=proc(f, sos, nsdponeg, g::list:=[])
  local s,i,j,idx,idxi,oneg;
  oneg := [1,op(g)]:
  s := 0; idx:=0;
for i from 1 to nops(g)+1 do
  idxi:=idx+nsdponeg[i]:
  for j from idx+1 to idxi do 
      if sos[2*j-1] < 0 then
         printf("Negative number => invalid sum of squares decomposition\n");
         return 0;
         error "Negative number => invalid sum of squares decomposition"; 
      else
          s := s + oneg[i]*sos[2*j-1]*sos[2*j]^2
      fi: 
  od:
  idx:=idxi:
  od:

  #MS: Il ne faut surtout pas faire comme ca pour des raisons d'efficacite.
  #Proceder par evaluation
  if not expand(f - s) = 0 then
    printf("Inexact sum of squares decomposition");
    return 0;
    error "Inexact sum of squares decomposition";
  else
    return 1:
  fi:
end;

BitSizeSos := proc (sos,X)
   return add(BitSizePol(p[1], X) + BitSizePolQuadr(p[2], X), p in sos);
end;

BitSizePolQuadr := proc(q,X)
  return BitRat(q[1]) + BitSizePol(q[2], X) + BitRat(q[3]);
end;

BitSizePolSeq3 := proc(listpol,X)
  return add(BitSizePol(p[1], X) + BitSizePolSeq(p[2], X), p in listpol);
end;

BitSizePolSeq := proc(listpol,X)
  return add(BitSizePol(p, X), p in listpol);
end;

BitSizePol:=proc(p, X)
  local res;
  res := [coeffs(expand(p),X)];
  return BitSizeSeq(res);
end;

BitSizeSeq:=proc(l)
  return add(BitRat(c), c in l);
end;

BitRat := proc(r)
  local n, d, res,rs;
  if type(r,rational) then rs :=r : else rs:=r^2: fi:
  if rs = 0 then return 1; fi;
  (n, d) := (abs(numer(rs)), abs(denom(rs)));
  if d = 1 then res :=  ilog2(n) + 1 else res := ilog2(n) + ilog2(d) + 2 fi;
  return res;
end;

benchRAGLib := proc(f,g::list:=[])
  local sys,i,ti,tf,sol;
  ti := time[real]():
  sys := [f < 0];
  for i from 1 to nops(g) do
    sys := [op(sys), expand(-g[i]) < 0]
  od:
  lprint(sys);
  sol := RAG:-HasRealSolutions(sys);
  tf := time[real]()-ti; 
  printf("time= %esecs\n",tf);
  return sol;
end;

benchSamplePoints := proc(f,g::list:=[])
  local sys,vars,R,P,i,ti,tf;
  ti := time[real]():
  sys := [f < 0];
  for i from 1 to nops(g) do
    sys := [op(sys), g[i] >= 0]
  od:
  vars:=[op(indets(f))]: R:=PolynomialRing(vars): P:=SamplePoints(sys,R);
  tf := time[real]()-ti; 
  printf("time= %esecs\n",tf);
  return P;
end;

printms := proc(ms)
  local i;
  for i from 1 to nops(ms) do
    printf("%d ",i): lprint(ms[i]):
  od:
  return;
end;
