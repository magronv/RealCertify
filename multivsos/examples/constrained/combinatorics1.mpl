#Example coming from combinatorics.

for i from 1 to 12 do
beta[i]:=cat('x',i);
od;

P:={-2*(2 + k + l + m + n)*beta[12] >= 0, (1 + l)*beta[12] >= 0,
 (1 + k)*beta[12] >= 0, (2*(2 + k + l + m)*beta[1])/3 +
   (2*(2 + k + l + m)*beta[2])/3 + (2*l*beta[3])/3 + (2*k*beta[4])/3 +
   (2*beta[5])/3 + (2*(1 + l)*beta[6])/3 + (2*(1 + l)*beta[7])/3 +
   (2*(1 + k)*beta[9])/3 + (2*(1 + k)*beta[10])/3 + (2*(m - n)*beta[11])/3 >=
  0, (1 + m)*beta[12] >= 0, (2*(2 + k + l + m)*beta[1])/3 - (2*beta[2])/3 +
   (2*l*beta[3])/3 + (2*(1 + k + l)*beta[4])/3 + (2*beta[5])/3 +
   (2*(1 + l)*beta[6])/3 + (2*(1 + l)*beta[8])/3 + (2*(k - n)*beta[9])/3 -
   (2*(1 + m)*beta[10])/3 + (2*(1 + m)*beta[11])/3 >= 0,
 (2*(2 + k + l + m)*beta[1])/3 - (2*beta[2])/3 + (2*l*beta[3])/3 -
   (2*beta[4])/3 + (2*beta[5])/3 + (2*(l - n)*beta[6])/3 -
   (2*(1 + m)*beta[7])/3 - (2*(1 + k)*beta[8])/3 + (2*(1 + k)*beta[9])/3 +
   (2*(1 + m)*beta[11])/3 >= 0, (-3 - k - l - m - n)*beta[1] - m*beta[2] -
   l*beta[3] - k*beta[4] - beta[5] + (-1 - l)*beta[6] + (-1 - k)*beta[9] +
   (-1 - m)*beta[11] >= 0, (1 + n)*beta[12] >= 0,
 (-2*(2 + n)*beta[1])/3 + (2*(1 + k + l)*beta[2])/3 + (2*l*beta[3])/3 +
   (2*(1 + k + l)*beta[4])/3 + (2*beta[5])/3 + (2*(1 + l)*beta[7])/3 +
   (2*(1 + l)*beta[8])/3 - (2*(1 + n)*beta[9])/3 + (2*(k - m)*beta[10])/3 -
   (2*(1 + n)*beta[11])/3 >= 0,
 (-2*(2 + n)*beta[1])/3 + (2*(1 + k + l)*beta[2])/3 + (2*l*beta[3])/3 -
   (2*beta[4])/3 + (2*beta[5])/3 - (2*(1 + n)*beta[6])/3 +
   (2*(l - m)*beta[7])/3 - (2*(1 + k)*beta[8])/3 + (2*(1 + k)*beta[10])/3 -
   (2*(1 + n)*beta[11])/3 >= 0, beta[1] + (-2 - k - l - m)*beta[2] -
   l*beta[3] - k*beta[4] - beta[5] + (-1 - l)*beta[7] + (-1 - k)*beta[10] +
   (1 + n)*beta[11] >= 0, (-2*(2 + n)*beta[1])/3 - (2*(2 + m)*beta[2])/3 +
   (2*l*beta[3])/3 + (2*l*beta[4])/3 + (2*beta[5])/3 -
   (2*(1 + n)*beta[6])/3 - (2*(1 + m)*beta[7])/3 - (2*(k - l)*beta[8])/3 -
   (2*(1 + n)*beta[9])/3 - (2*(1 + m)*beta[10])/3 >= 0,
 beta[1] + beta[2] - l*beta[3] + (-1 - k - l)*beta[4] - beta[5] +
   (-1 - l)*beta[8] + (1 + n)*beta[9] + (1 + m)*beta[10] >= 0,
 beta[1] + beta[2] - l*beta[3] + beta[4] - beta[5] + (1 + n)*beta[6] +
   (1 + m)*beta[7] + (1 + k)*beta[8] >= 0,
 n*beta[1] + m*beta[2] + l*beta[3] + k*beta[4] + beta[5] < 0};


P:=[-2*(2 + k + l + m + n)>=0, beta[12] >= 0, (1 + l)>=0,
 (1 + k)>=0, beta[12] >= 0, (2*(2 + k + l + m)*beta[1])/3 +
   (2*(2 + k + l + m)*beta[2])/3 + (2*l*beta[3])/3 + (2*k*beta[4])/3 +
   (2*beta[5])/3 + (2*(1 + l)*beta[6])/3 + (2*(1 + l)*beta[7])/3 +
   (2*(1 + k)*beta[9])/3 + (2*(1 + k)*beta[10])/3 + (2*(m - n)*beta[11])/3 >=
  0, (1 + m) >= 0, (2*(2 + k + l + m)*beta[1])/3 - (2*beta[2])/3 +
   (2*l*beta[3])/3 + (2*(1 + k + l)*beta[4])/3 + (2*beta[5])/3 +
   (2*(1 + l)*beta[6])/3 + (2*(1 + l)*beta[8])/3 + (2*(k - n)*beta[9])/3 -
   (2*(1 + m)*beta[10])/3 + (2*(1 + m)*beta[11])/3 >= 0,
 (2*(2 + k + l + m)*beta[1])/3 - (2*beta[2])/3 + (2*l*beta[3])/3 -
   (2*beta[4])/3 + (2*beta[5])/3 + (2*(l - n)*beta[6])/3 -
   (2*(1 + m)*beta[7])/3 - (2*(1 + k)*beta[8])/3 + (2*(1 + k)*beta[9])/3 +
   (2*(1 + m)*beta[11])/3 >= 0, (-3 - k - l - m - n)*beta[1] - m*beta[2] -
   l*beta[3] - k*beta[4] - beta[5] + (-1 - l)*beta[6] + (-1 - k)*beta[9] +
   (-1 - m)*beta[11] >= 0, (1 + n) >= 0,
 (-2*(2 + n)*beta[1])/3 + (2*(1 + k + l)*beta[2])/3 + (2*l*beta[3])/3 +
   (2*(1 + k + l)*beta[4])/3 + (2*beta[5])/3 + (2*(1 + l)*beta[7])/3 +
   (2*(1 + l)*beta[8])/3 - (2*(1 + n)*beta[9])/3 + (2*(k - m)*beta[10])/3 -
   (2*(1 + n)*beta[11])/3 >= 0,
 (-2*(2 + n)*beta[1])/3 + (2*(1 + k + l)*beta[2])/3 + (2*l*beta[3])/3 -
   (2*beta[4])/3 + (2*beta[5])/3 - (2*(1 + n)*beta[6])/3 +
   (2*(l - m)*beta[7])/3 - (2*(1 + k)*beta[8])/3 + (2*(1 + k)*beta[10])/3 -
   (2*(1 + n)*beta[11])/3 >= 0, beta[1] + (-2 - k - l - m)*beta[2] -
   l*beta[3] - k*beta[4] - beta[5] + (-1 - l)*beta[7] + (-1 - k)*beta[10] +
   (1 + n)*beta[11] >= 0, (-2*(2 + n)*beta[1])/3 - (2*(2 + m)*beta[2])/3 +
   (2*l*beta[3])/3 + (2*l*beta[4])/3 + (2*beta[5])/3 -
   (2*(1 + n)*beta[6])/3 - (2*(1 + m)*beta[7])/3 - (2*(k - l)*beta[8])/3 -
   (2*(1 + n)*beta[9])/3 - (2*(1 + m)*beta[10])/3 >= 0,
 beta[1] + beta[2] - l*beta[3] + (-1 - k - l)*beta[4] - beta[5] +
   (-1 - l)*beta[8] + (1 + n)*beta[9] + (1 + m)*beta[10] >= 0,
 beta[1] + beta[2] - l*beta[3] + beta[4] - beta[5] + (1 + n)*beta[6] +
   (1 + m)*beta[7] + (1 + k)*beta[8] >= 0,
 n*beta[1] + m*beta[2] + l*beta[3] + k*beta[4] + beta[5] < 0];


F:=map(E->rhs(E)-lhs(E), P);
F:=[op(F)];
