ineq := [Y, 1-Y, b, d, f, h, 1-b-h, 1-d-f, Y-b-d, Y-f-h, Y-b-f, Y-d-h]:
vars := [Y, b, d, f, h]:
Penta2 := [op(ineq), -5*Y/4 + b^2 + d*(Y-b) + f*(1-d) + h*(1-b)]:
