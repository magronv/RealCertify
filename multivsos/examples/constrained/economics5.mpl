#Extracted from
#Mulligan, C. B., Bradford, R., Davenport, J. H., England, M., & Tonks, Z. (2018). Non-linear Real Arithmetic Benchmarks derived from Automated Reasoning in Economics (No. w24602). National Bureau of Economic Research.
v10:=1:
v9:=1:
v3:=1:

Equations:=[v1*v10+v2*v7+v3*v5,
     v1*v11+v2*v8+v3*v7,
    v1*v12+v10*v3+v11*v2]:

Inequalities:=[v1,v2,v3,v4,v6,v9,
        2*v11*v6*v9-v12*v6^2-v8*v9^2,
        2*v10*v6*(v11*v4+v7*v9)+v9*(2*v11*v4*v7-2*v11*v5*v6+v5*v8*v9)+v12*(v4^2*v8-2*v4*v6*v7+v5*v6^2) -
               (v10^2*v6^2+2*v10*v4*v8*v9+v11^2*v4^2+v7^2*v9^2),
               v11^2-v12*v8
       ];


