#Example extracted from
#Everett, H., Lazard, D., Lazard, S., Safey El Din, M. (2009). The Voronoi
# diagram of three lines. Discrete & Computational Geometry, 42(1), 94-130.

#Example difficult, probably not in the interior of the SOS cone.
pol:= 32*a^2*u^3*alpha^2+4*u*x^2*a^2-2*a*alpha*beta-2*x*beta*a^3+2*y*beta*a^2-16*a^3*u^3*alpha*beta+16*a^2*u^3*x*alpha+16*a^2*u^3*y*beta-16*a*u^3*alpha*beta-8*u^2*x*beta*a^3-24*u^2*a*alpha*beta+24*u^2*y*beta*a^2-24*u^2*alpha*beta*a^3-8*u^2*a*x*beta+24*u^2*x*alpha*a^2-8*u^2*y*a^3*alpha-8*u^2*a*y*alpha-12*u*alpha*beta*a^3-8*u*y*a^3*alpha+4*u*alpha*a^4*x+12*u*y*beta*a^2-4*u*y*a^3*x-8*u*a*x*beta+12*u*x*alpha*a^2-4*u*a*x*y-12*u*a*alpha*beta-8*u*a*y*alpha-8*u*x*beta*a^3+a^4*alpha^2+y^2*a^2+x^2*a^2+a^2*beta^2+a^2*alpha^2+32*a^2*u^3+4*u^2*beta^2+16*u^2*a^2+2*beta*y-2*alpha*beta*a^3-2*a*x*beta+2*x*alpha*a^2-2*y*a^3*alpha-2*a*y*alpha+beta^2+16*a^2*u^4+16*a^2*u^4*alpha^2+16*a^2*u^4*beta^2+32*a^2*u^3*beta^2+24*u^2*a^2*beta^2+24*u^2*a^2*alpha^2+4*u^2*a^4*alpha^2+4*u^2*y^2*a^2+4*u^2*x^2*a^2+2*alpha*a^4*x-2*y*a^3*x-2*a*x*y+4*u*a^4*alpha^2+4*u*y^2*a^2+8*u*a^2*alpha^2+8*u*a^2*beta^2+4*u*beta*y+4*u*beta^2+y^2+a^4*x^2;

#read "../../multivsos_moi.mm":
#multivsos(pol):
infolevel[Groebner:-Basis]:=4:
st:=time(): sols:=RAG:-HasRealSolutions([pol<0]): time()-st;